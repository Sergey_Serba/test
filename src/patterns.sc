patterns:
    $Any3Words = [$AnyWord] [$AnyWord] [$AnyWord]
    $skip = (пропус*/не [$Any3Words] (надо/знаю/говорить/указывать/вводить/используем/скажу/указ*/укажу/хоч*/напиш*/введу/[$AnyWord] вводи*)/нет/отстань/дальше/следующ* вопрос*)

    $name = $entity<NamesRu> || converter = function(pt) {var id = pt.NamesRu[0].value; return NamesRu[id].value;}

    $strongLastName = $regexp<[А-яЁё]+((-|')[А-яЁё]+)?((ов)|(ова)|(ев)|(ева)|(ин)|(ина)|(ын)|(ына)|(ской)|(ский)|(ская)|(цкой)|(цкая)|(цкий)|(их)|(ых)|(енко)|(ко)|(ук)|(юк)|(ман)|(швили)|(ян)|(янц)|(ава)|(ты)|(ти)|(фельд)|(штерн)|(ес)|(ез)|(айтис)|(айте)|(сон))>
    $middleName = $regexp<[А-яЁё]+((ич)|(вна)|(чна))>
    $lastName = $regexp<[А-яЁё]+((-|')[А-яЁё]+)?>

    $salary = (зарплат*/заработн* плат*/зп/средн* * (доход*/заработ*)/доход*/расчет)

    $date = ($DateAbsoluteWithoutWeekdays|$DateDayMonth) || converter = $converters.dateConverter

    $DateDayMonth = $regexp<\d+((\.|\/)((0)?(1(0|1|2)|1|2|3|4|5|6|7|8|9)))> 

    $DateYearNumberTts = $regexp<2(\s|.)0\d{2}> || converter = $converters.dateYearTtsConverter

    $DateAbsoluteWithoutWeekdays =  (
        ($DateDayNumber::DateDayNumeric|$DateDayOrderNumber::DateDayNumeric) [числа] {($DateMonth) [месяц*]} [ [в] $DateYearNumber::DateYearNumeric [год*]] |
        {($DateDayNumber::DateDayNumeric [числа] $DateMonth [месяц*] [ $DateYearTwoNumber::DateYearNumeric год* |($DateDayDigit)($DateMonth)| $DateYearNumber::DateYearNumeric [года] | $DateYearNumeric [год*] ])} |
        $DateDayNumber::DateDayNumeric (числа|число) |
        $DateDayOrderNumber::DateDayNumeric [числа|число] |        
        [это* [день]] $DateDayNumber::DateDayNumeric [число] $DateMonth [месяц*] [ $DateYearNumber::DateYearNumeric [года] | $DateYearNumeric [год*] | $DateYearTwoNumber::DateYearNumeric год* ] | 
        $DateRelativeYear $DateDayNumber::DateDayNumeric [числа] $DateMonth [месяц*] |
        $DateDayNumber::DateDayNumeric [числа] $DateMonth [месяц*] [в] $DateRelativeYear |
        $DateDayNumber::DateDayNumeric [числа] $DateRelativeMonth [месяц*] |
        $DateHoliday
        ) ||  converter = $converters.absoluteDateConverter;

    $DateDayMonthYearDigit = $regexp<^(?:(?:31(\/|-|\.|\,)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.|\,)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.|\,)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.|\,)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$>[г] || converter = $converters.timeConverterDateMonthYear;

    $DateFuturePastModifierNew = (
        (в (этом|эту|текущем) | этого | сего | текущего|ближайш* ):0 | 
        (следующ*) :1 |
        прошл* :-1 |
        позапрошл* :-2 |
        через (~один|$Number|*):1 
        ) ||  converter = $converters.numberConverterMultiply;

    $DateRelativeMonthNew = (
        $DateFuturePastModifierNew месяц* |
        [$Number] месяц* назад:-1
        ) ||  converter = $converters.relativeMonthConverter;

    $recipient = (~получатель/~получать)
    $payer = (платель*/плачу/оплач*)

    $DateMonthAndDigits = (
        (~январь|январ*|янв|january|jan|$one|01):1 |
        (~февраль|феврал*|фев|february|feb|$two|02):2 |
        (~март|март*|мар|march|mar|$three|03):3 |
        (~апрель|апрел*|апр|april|apr|$four|04):4 |
        (~май|май|мая|мае|may|$five|05):5 |
        (~июнь|июнь|июня|июне|июн|june|jun|$six|06):6 |
        (~июль|июль|июля|июле|июл|july|jul|$seven|07):7 |
        (~август|август*|авг|august|aug|8|08|восьмой|восемь):8 |
        (~сентябрь|сентябр*|сент|сен|september|sep|9|09|девять|девятый):9 |
        (~октябрь|октябр*|окт|october|oct|10|десятый|десять):10 |
        (~ноябрь|ноябр*|ноя|нояб|november|nov|11|одиннадцат*):11 |
        (~декабрь|декабр*|дек|december|dec|12|двенадцат*):12 ) ||  converter = $converters.numberConverterValue

    $next = (далее/еще/дальше/следующ*/продолж*)

    $hr = (([контакт*] (центр*/менеджер*) [по/для] [поддерж*] персонал*)/персональщ*/расчетчи*/табельщи*/кадрови*)
    $support = ((контакт* центр*)/контакт-центр*/техпод*/(тех* поддерж*)/*саппорт*)
    $quite = (очень/вполне/совсем)
    $colleague = (сотрудни*/работн*/начальни*/руководител*/специалист*/коллег*/сослужив*/товарищ*/собрат*/напарни*)
    $oneWindow = (СЕО/всьо/всього/seo/всего/всео/всё/село/СИО/села/(служ* един* окн*)/$regexp<(C|c|С|с)\&(A|a|А|а)>):СЕО
    $phoneNumberdigits = $regexp<^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,11}(\s*)?$> || converter = $converters.phoneNumberInDigitsConverter
