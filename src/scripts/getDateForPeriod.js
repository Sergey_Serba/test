$global.$converters = $global.$converters || {};
var cnv = $global.$converters;

cnv.dateConverter = function(parseTree) {
    var value = cnv.propagateConverter(parseTree);
    var res;
    if (value && value.day && value.month) {
        res = {
            day: value.day,
            month: value.month,
            year: value.year
        };
    } else {
        res = {
            day: parseInt(parseTree.words[0], 10),
            month: parseInt(parseTree.words[2], 10),
            year: parseInt(parseTree.words[4], 10)
        };
    }
    return res;
};

cnv.dateYearTtsConverter = function(parseTree) {
    return parseTree.words.length === 2 ? parseTree.words[0] + parseTree.words[1] : parseTree.words[0] + parseTree.words[2];
};

cnv.timeConverterDateMonthYear = function(parseTree) {
    var value = {
        day: parseInt(parseTree.words[0], 10),
        month: parseInt(parseTree.words[2], 10),
        year: parseInt(parseTree.words[4], 10)
    };
    return value;
};

cnv.phoneNumberInDigitsConverter = function(pt) {
    var roughNumber = pt.text;
    var phoneNumber = "";
    for (var i = 0; i < roughNumber.length; ++i) {
        if (!isNaN(parseInt(roughNumber.charAt(i)))) {
            phoneNumber += roughNumber.charAt(i);
        }
    }
    if (phoneNumber.length === 10) {
        phoneNumber = "+7" + phoneNumber;
    } else if (phoneNumber.length === 11 && phoneNumber.charAt(0) === "7") {
        phoneNumber = "+" + phoneNumber;
    } else if (phoneNumber.length === 11) {
        phoneNumber = "+7" + phoneNumber.substr(1);
    }
    if (phoneNumber.length === 13 || (phoneNumber.length === 12 && phoneNumber.charAt(1) === "7" && phoneNumber.charAt(2) === "7")) {
        return false;
    }
    return phoneNumber
}


var DateRecognition = (function() {

    function checkPeriodDates(start, end, yearPrescribed) {
        $.temp.start = start;
        $.temp.end = end;
        var startDay = start.split(".")[0];
        var endDay = end.split(".")[0];

        // даты корректны
        var startIsValid = moment(start, "DD.MM.YYYY").isValid() && startDay.length === 2;
        var endIsValid = moment(end, "DD.MM.YYYY").isValid() && endDay.length === 2;

        var startBeforeEnd = moment(start, "DD.MM.YYYY").isSameOrBefore(moment(end, "DD.MM.YYYY"));
        var endInTheFuture = moment(moment(end, "DD.MM.YYYY")).isSameOrBefore(currentDate());
        var endtInTheFutureAfter = moment(moment(end, "DD.MM.YYYY")).isSameOrAfter(currentDate());
        var startInTheFuture = moment(moment(start, "DD.MM.YYYY")).isSameOrAfter(currentDate());
        var startInPast = moment(moment(start, "DD.MM.YYYY")).isAfter(currentDate());
        var endInPast = moment(moment(end, "DD.MM.YYYY")).isAfter(currentDate());
        if ($.temp.shouldBeFuture) {
            if (startIsValid && endIsValid && (startInPast || endInPast) && !yearPrescribed) {
                $.session.startDate = startInTheFuture || (startInPast && endInPast) ? start.substring(0, start.length - 4) + (currentDate().year()) : start;
                $.session.endDate = endInPast ? end.substring(0, end.length - 4) + (currentDate().year()) : end;
                $.session.rfsDuration = $.session.startDate + " - " + $.session.endDate;
            }
            if (!startIsValid || !endIsValid || !startBeforeEnd || !startInTheFuture || !endtInTheFutureAfter) {
                start = moment(start, "DD.MM.YYYY").add(1, "years");
                end = moment(end, "DD.MM.YYYY").add(1, "years");
                var startIsValid = moment(start, "DD.MM.YYYY").isValid() && startDay.length === 2;
                var endIsValid = moment(end, "DD.MM.YYYY").isValid() && endDay.length === 2;
                var startBeforeEnd = moment(start, "DD.MM.YYYY").isSameOrBefore(moment(end, "DD.MM.YYYY"));
                var endtInTheFutureAfter = moment(moment(end, "DD.MM.YYYY")).isSameOrAfter(currentDate());
                var startInTheFuture = moment(moment(start, "DD.MM.YYYY")).isSameOrAfter(currentDate());
                if (!startIsValid || !endIsValid || !startBeforeEnd || !startInTheFuture || !endtInTheFutureAfter) {
                    DateRecognition.clearDates();
                } else {
                    $.session.startDate = $.session.startDate.substring(0, $.temp.start.length - 4) + (currentDate().year() + 1);
                    $.session.endDate = $.session.endDate.substring(0, $.temp.end.length - 4) + (currentDate().year() + 1);
                    $.session.rfsDuration = $.session.startDate + " - " + $.session.endDate;
                }
            }
        } else {
            if (startIsValid && endIsValid && (startInPast || endInPast) && !yearPrescribed) {
                $.session.startDate = startInPast || (!startInPast && endInPast) ? start.substring(0, start.length - 4) + (currentDate().year() - 1) : start;
                $.session.endDate = endInPast ? end.substring(0, end.length - 4) + (currentDate().year() - 1) : end;
                $.session.rfsDuration = $.session.startDate + " - " + $.session.endDate;
            }
            if (!startIsValid || !endIsValid || !startBeforeEnd || !endInTheFuture) {
                DateRecognition.clearDates();
            }
        }
    }

    function getMonth(startDay, endDay) {
        var months;
        var currentDay = currentDate().date();
        var currentMonth = currentDate().month() + 1;
        if (currentDay < startDay && currentDay < endDay) {
            months = {"startMonth": currentMonth - 1, "endMonth": currentMonth - 1};
        }
        if ($.temp.shouldBeFuture) {
            if (currentDay < startDay && currentDay > endDay) {
                months = {"startMonth": currentMonth + 1, "endMonth": currentMonth + 1};
            } else {
                months = {"startMonth": currentMonth, "endMonth": currentMonth + 1};
            }
            return months;
        } else {
            if (currentDay < startDay && currentDay > endDay) {
                months = {"startMonth": currentMonth - 1, "endMonth": currentMonth};
            } else {
                months = {"startMonth": currentMonth, "endMonth": currentMonth};
            }
            return months;
        }
    }

    function getDoubleDate(number) {
        return number.toString().length === 1 ? "0" + number : number;
    }

    function getFullYear(number) {
        return number.toString().length === 2 ? "20" + number : number;
    }

    function clearDates() {
        delete $.session.rfsDuration;
        delete $.session.rfsDurationTts;
        delete $.session.startDate;
        delete $.session.endDate;
    }
    function getCurrentDateFormated() {
        return moment($jsapi.currentTime()).format("DD.MM.YYYY");
    }
    function addDaysMonthToDate($parseTree, date, amount) {
        return currentDate().add(parseInt($parseTree[date][0].value, 10), amount).locale("ru").format("DD.MM.YYYY");
    }

    function getRelDate($parseTree, duration) {
        if (($parseTree.value && $parseTree.value.day && $parseTree.value.month) || ($parseTree._DateDayMonthYearDigit && $parseTree._DateDayMonthYearDigit.day && $parseTree._DateDayMonthYearDigit.month)) {
            if ($parseTree.DateDayMonthYearDigit) {
                var day = $parseTree._DateDayMonthYearDigit.day.toString();
                var month = $parseTree._DateDayMonthYearDigit.month.toString();
            } else {
                var day = $parseTree.value.day.toString();
                var month = $parseTree.value.month.toString();
            }
            var nowYear = currentDate().format("YYYY");
            day = DateRecognition.getDoubleDate(day);
            month = DateRecognition.getDoubleDate(month);
            if (duration) {
                $.session.startDate = day + "." + month + "." + nowYear;
                $.session.endDate = DateRecognition.getCurrentDateFormated();
            } else {
                $.session.date = day + "." + month + "." + nowYear;
            }
        }
        if (($parseTree.value && $parseTree.value.year) || ($parseTree._DateDayMonthYearDigit && $parseTree._DateDayMonthYearDigit.year)) {
            if ($parseTree._DateDayMonthYearDigit && $parseTree._DateDayMonthYearDigit.year) {
                var year = $parseTree._DateDayMonthYearDigit.year.toString();
            } else {
                var year = $parseTree.value.year.toString();
            }
            if (duration) {
                $.session.startDate = day + "." + month + "." + year;
                $.session.endDate = DateRecognition.getCurrentDateFormated();
            } else {
                $.session.date = day + "." + month + "." + year;
            }
        }
        if ($parseTree.DateRelativeDay) {
            if (duration) {
                $.session.startDate = DateRecognition.addDaysMonthToDate($parseTree, "DateRelativeDay", "days");
                $.session.endDate = DateRecognition.getCurrentDateFormated();
            } else {
                $.session.date = DateRecognition.addDaysMonthToDate($parseTree, "DateRelativeDay", "days");
            }
        } else if ($parseTree.DateTimeRelativeWeeks) {
            if (duration) {
                $.session.startDate = DateRecognition.addDaysMonthToDate($parseTree, "DateTimeRelativeWeeks", "days");
                $.session.endDate = DateRecognition.getCurrentDateFormated();
            } else {
                $.session.date = DateRecognition.addDaysMonthToDate($parseTree, "DateTimeRelativeWeeks", "days");
            }
        } else if ($parseTree.DateRelativeMonthNew) {
            if (duration) {
                $.session.startDate = DateRecognition.addDaysMonthToDate($parseTree, "DateRelativeMonthNew", "month");
                $.session.endDate = DateRecognition.getCurrentDateFormated();
            } else {
                $.session.date = DateRecognition.addDaysMonthToDate($parseTree, "DateRelativeMonthNew", "month");
            }
        }
        if (duration) {
            $.session.rfsDuration = $.session.startDate + " - " + $.session.endDate;
            return DateRecognition.checkPeriodDates($.session.startDate, $.session.endDate);
        }
    }


    function getPeriod($parseTree) {
        var yearPrescribed;
        if ($parseTree.date && Object.keys($parseTree.date).length === 2) {
            var start = $parseTree.date[0].value;
            var end = $parseTree.date[1].value;
            var startMonth = start.month ? start.month : end.month ? end.month : currentDate().month() + 1;
            var endMonth = end.month ? end.month : currentDate().month() + 1;
            var startYear = start.year ? start.year : currentDate().year();
            var endYear = end.year ? end.year : currentDate().year();
            yearPrescribed = start.year || end.year;

            $.session.startDate = DateRecognition.getDoubleDate(start.day) + "." + DateRecognition.getDoubleDate(startMonth) + "." + DateRecognition.getFullYear(startYear);
            $.session.endDate = DateRecognition.getDoubleDate(end.day) + "." + DateRecognition.getDoubleDate(endMonth) + "." + DateRecognition.getFullYear(endYear);
        } else if ($parseTree.Number && $parseTree.date) {
            var end = $parseTree.date[0].value;
            var endYear = end.year ? end.year : currentDate().year();
            yearPrescribed = end.year;
            var startDay = $parseTree.Number[0].value;
            if ($parseTree.Number[0].value.toString().length > 2) {
                var startDay = $parseTree.Number[0].value.toString().substr(0, 2);
            }
            $.session.startDate = DateRecognition.getDoubleDate(startDay) + "." + DateRecognition.getDoubleDate(end.month) + "." + DateRecognition.getFullYear(endYear);
            $.session.endDate = DateRecognition.getDoubleDate(end.day) + "." + DateRecognition.getDoubleDate(end.month) + "." + DateRecognition.getFullYear(endYear);
        } else if ($parseTree.date && Object.keys($parseTree.date).length === 1) {
            var start = $parseTree.date[0].value;
            var startMonth = start.month ? start.month : currentDate().month() + 1;
            var startYear = start.year ? start.year : currentDate().year();
            yearPrescribed = start.year;

            $.session.startDate = DateRecognition.getDoubleDate(start.day) + "." + DateRecognition.getDoubleDate(startMonth) + "." + DateRecognition.getFullYear(startYear);
            $.session.endDate = DateRecognition.getCurrentDateFormated();
        } else if ($parseTree.Number && Object.keys($parseTree.Number).length === 2) {
            var startDay = $parseTree.Number[0].value.toString().substr(0, 2);
            var endDay = $parseTree.Number[1].value;
            if ($parseTree.Number[0].value.toString().length > 2) {
                var startDay = $parseTree.Number[0].value.toString().substr(0, 2);
                var endDay = $parseTree.Number[1].value.toString().substr(0, 2);
            }
            var months = DateRecognition.getMonth(startDay, endDay);
            yearPrescribed = false;

            $.session.startDate = DateRecognition.getDoubleDate(startDay) + "." + DateRecognition.getDoubleDate(months.startMonth) + "." + currentDate().year();
            $.session.endDate = DateRecognition.getDoubleDate(endDay) + "." + DateRecognition.getDoubleDate(months.endMonth) + "." + currentDate().year();
        } else if ($parseTree.Number && Object.keys($parseTree.Number).length === 1) {
            var startDay = $parseTree.Number[0].value.toString().substr(0, 2);
            var endDay = $parseTree.Number[0].value;
            if ($parseTree.Number[0].value.toString().length > 2) {
                var startDay = $parseTree.Number[0].value.toString().substr(0, 2);
                var endDay = startDay;
            }
            var months = DateRecognition.getMonth(startDay, endDay);
            yearPrescribed = false;

            $.session.startDate = DateRecognition.getDoubleDate(startDay) + "." + DateRecognition.getDoubleDate(months.startMonth) + "." + currentDate().year();
            $.session.endDate = DateRecognition.getDoubleDate(endDay) + "." + DateRecognition.getDoubleDate(months.endMonth) + "." + currentDate().year();
        }
        $.session.rfsDuration = $.session.startDate + " - " + $.session.endDate;
        return DateRecognition.checkPeriodDates($.session.startDate, $.session.endDate, yearPrescribed);
    }

    // 2-ндфл
    function getYears($parseTree) {
        var thisYear = new Date().getFullYear();
        if ($parseTree._Root == 2) {
            $.session.rfsYears = thisYear;
        } else {
            if ($parseTree.DateYearNumber && Object.keys($parseTree.DateYearNumber).length === 2) {
                $.session.startYear = $parseTree.DateYearNumber[0].value;
                $.session.endYear = $parseTree.DateYearNumber[1].value;
            } else if ($parseTree.DateYearNumeric && Object.keys($parseTree.DateYearNumeric).length === 2) {
                $.session.startYear = $parseTree.DateYearNumeric[0].value;
                $.session.endYear = $parseTree.DateYearNumeric[1].value;
            } else if ($parseTree.DateYearTwoNumber && Object.keys($parseTree.DateYearTwoNumber).length === 2) {
                $.session.startYear = DateRecognition.getFullYear($parseTree.DateYearTwoNumber[0].value);
                $.session.endYear = DateRecognition.getFullYear($parseTree.DateYearTwoNumber[1].value);
            } else if ($parseTree.DateYearNumberTts && Object.keys($parseTree.DateYearNumberTts).length === 2) {
                $.session.startYear = $parseTree.DateYearNumberTts[0].value;
                $.session.endYear = $parseTree.DateYearNumberTts[1].value;
            } else {
                if ($parseTree.DateRelativeYear) {
                    $.session.rfsYears = currentDate().add($parseTree.DateRelativeYear[0].value, 'years').year();
                } else {
                    $.session.rfsYears = $parseTree._DateYearNumber || $parseTree._DateYearNumeric || $parseTree._DateYearNumberTts || "20" + $parseTree._DateYearTwoNumber;
                }
                if (!$.temp.couldBeFuture && thisYear - $.session.rfsYears < 0) {
                    delete $.session.rfsYears;
                    $.temp.oneAnswer = true;
                    $reactions.transition("../LocalCatchAll");
                }
            }
            if (!$.temp.couldBeFuture && !$.session.rfsYears && $.session.startYear && $.session.endYear) {
                if ($.session.endYear - $.session.startYear >= 5 || thisYear - $.session.endYear >= 5 || thisYear - $.session.startYear >= 5
                    || thisYear - $.session.startYear < 0 || $.session.endYear > thisYear) {
                    $.temp.oneAnswer = true;
                    $reactions.answer("Период справки не должен включать текущий месяц, а давность не должна превышать 4 года.");
                } else if ($.temp.onlyYears) {
                    $.session.rfsYears = $.session.startYear + " - " + $.session.endYear;
                } else {
                    if ($.session.startYear > $.session.endYear) {
                        $.temp.endDateBeforeStart = true;
                        $.temp.oneAnswer = true;
                        $reactions.transition("../LocalCatchAll");
                    } else {
                        $.session.rfsYears = "01." + $.session.startYear + " - 12." + $.session.endYear;
                    }
                }
            } else {
                if (!$.session.rfsYears && $.session.startYear && $.session.endYear) {
                    if ($.session.startYear > $.session.endYear) {
                        $.temp.endDateBeforeStart = true;
                        $.temp.oneAnswer = true;
                        $reactions.transition("../LocalCatchAll");
                    } else {
                        $.session.rfsYears = "01." + $.session.startYear + " - 12." + $.session.endYear;
                    }
                }
            }
        }
    }

    // Алименты
    // Средний доход
    // Копия исполнительного листа
    function getMonthsAndYears($parseTree) {
        $.temp.thisYear = new Date().getFullYear();
        $.temp.thisMonth = new Date().getMonth() + 1;
        if ($parseTree.DateYearNumber && Object.keys($parseTree.DateYearNumber).length === 2) {
            $.temp.startYear = $parseTree.DateYearNumber[0].value;
            $.temp.endYear = $parseTree.DateYearNumber[1].value;
        } else if ($parseTree.DateYearNumeric && Object.keys($parseTree.DateYearNumeric).length === 2) {
            $.temp.startYear = $parseTree.DateYearNumeric[0].value;
            $.temp.endYear = $parseTree.DateYearNumeric[1].value;
        } else if ($parseTree.DateYearTwoNumber && Object.keys($parseTree.DateYearTwoNumber).length === 2) {
            $.temp.startYear = DateRecognition.getFullYear($parseTree.DateYearTwoNumber[0].value);
            $.temp.endYear = DateRecognition.getFullYear($parseTree.DateYearTwoNumber[1].value);
        } else {
            if ($parseTree.DateRelativeYear) {
                $.temp.startYear = currentDate().add($parseTree.DateRelativeYear[0].value, 'years').year();
                $.temp.endYear = $.temp.startYear;
            } else {
                $.temp.startYear = $parseTree._DateYearNumber || $parseTree._DateYearNumeric || "20" + $parseTree._DateYearTwoNumber;
                $.temp.endYear = $.temp.startYear;
            }
        }
        if ($.temp.startYear == "20undefined") {
            $.temp.startYear = $.temp.thisYear;
            $.temp.endYear = $.temp.startYear;
        }
        if ($parseTree.DateMonthAndDigits && Object.keys($parseTree.DateMonthAndDigits).length === 2) {
            $.temp.startMonth = $parseTree.DateMonthAndDigits[0].value;
            $.temp.endMonth = $parseTree.DateMonthAndDigits[1].value;
        } else {
            $.temp.startMonth = $parseTree.DateMonthAndDigits[0].value;
            $.temp.endMonth = $parseTree.DateMonthAndDigits[0].value;
        }
        if ($.temp.startMonth > $.temp.endMonth && $.temp.endYear == $.temp.startYear) {
            var startMonth = $.temp.endMonth;
            var endMonth = $.temp.startMonth;
            $.temp.endMonth = endMonth;
            $.temp.startMonth = startMonth;
        }
        if ($.temp.endYear - $.temp.startYear >= 5 || $.temp.thisYear - $.temp.endYear >= 5
            || $.temp.thisYear - $.temp.startYear >= 5) {
            $.temp.oneAnswer = true;
            $reactions.transition("../LocalCatchAll");
        } else if ($.temp.startYear > $.temp.endYear) {
            $.temp.endDateBeforeStart = true;
            $.temp.oneAnswer = true;
            $reactions.transition("../LocalCatchAll");
        } else {
            if ($.temp.oneDate || ($.temp.startYear == $.temp.endYear && $.temp.startMonth == $.temp.endMonth)) {
                $.session.rfsDuration = DateRecognition.getDoubleDate($.temp.startMonth) + "." + $.temp.startYear;
                $.session.rfsDurationTts = getMonthsText($.session.rfsDuration);
            } else {
                $.session.rfsDuration = DateRecognition.getDoubleDate($.temp.startMonth) + "." + $.temp.startYear + " - " + DateRecognition.getDoubleDate($.temp.endMonth) + "." + $.temp.endYear;
                $.session.rfsDurationTts = getMonthsText(DateRecognition.getDoubleDate($.temp.startMonth) + "." + $.temp.startYear) + " - " + getMonthsText(DateRecognition.getDoubleDate($.temp.endMonth) + "." + $.temp.endYear);
            }
        }
    }
    return {
            getMonthsAndYears: getMonthsAndYears,
            getYears: getYears,
            getPeriod: getPeriod,
            getRelDate: getRelDate,
            checkPeriodDates:checkPeriodDates,
            getMonth:getMonth,
            getDoubleDate:getDoubleDate,
            getFullYear:getFullYear,
            clearDates:clearDates,
            getCurrentDateFormated:getCurrentDateFormated,
            addDaysMonthToDate:addDaysMonthToDate
        };
})();


