function rankTts(text) {
    var rank = {
        "1": "первый",
        "2": "второй",
        "3": "третий",
        "4": "четвертый",
        "5": "пятый",
        "6": "шестой",
        "7": "седьмой",
        "8": "восьмой",
        "9": "девятый",
        "10": "десятый"
    };
    var res = text.replace(/(\d{1,2}) разряд/, function(m) {
        return m[1] === "0" ? rank["10"] + " разряд" : rank[m[0]] + " разряд";
    });
    return res;
}

function getYearsTts(propertyName) {
    function getParentheses(match) {
        return "(" + match + ")";
    }
    return propertyName.toString().replace(/\d{4}/gm, getParentheses);
}

function readPhone(tel) {
    tel = tel.replace(/\(|\)|-/gm, "");
    var telStrTts = "";
    if (tel.length === 11) {
        telStrTts = tel[0] + " " + tel.slice(1, 4) + " " + tel.slice(4, 7) + " " + tel.slice(7, 9) + " " + tel.slice(9, 11);
    } else if (tel.length === 7) {
        telStrTts = tel.slice(0, 3) + " " + tel.slice(3, 5) + " " + tel.slice(5, 7);
    } else if (tel.length === 12) {
        telStrTts = tel.slice(0, 2) + " " + tel.slice(2, 5) + " " + tel.slice(5, 8) + " " + tel.slice(8, 10) + " " + tel.slice(10, 12);
    } else {
        telStrTts = tel.split("").join(" ");
    }
    return telStrTts;
}

var systemAnswer = $reactions.answer;

function appendAnswer(arg) {
    if (typeof (arg) !== "string") {
        throw "appendAnswer argument must be a string";
    }

    var text = arg;
    text = $reactions.template(text, $jsapi.context());

    var $response = $jsapi.context().response;

    // 2. fill tts in reply
    $response.replies = $response.replies || [{
        type: "text"
    }];

    var lastReply = $response.replies[$response.replies.length - 1];
    lastReply.text = lastReply.text ? lastReply.text + " " + text : text;

    $response.answer = $response.answer ? $response.answer + " " + text : text;
}

function answer(arg) {
    if (arg === null || arg === undefined) {
        return;
    }
    if (typeof arg === "object") {
        var text = arg.value;
        text = resolveInlineDictionary(resolveVariables(resolveInlineDictionary(text)));
        if (!arg.append) {
            systemAnswer(text);
        } else {
            if (typeof (arg.value) !== "string") {
                log("function_answer: " + JSON.stringify(arg));
            }
            appendAnswer(text);
        }
        tts(arg.tts || text);
        return;
    }

    if (arguments.length > 1) {
        var idx = $reactions.random(arguments.length);
        answer(arguments[idx]);
        return;
    }

    if (typeof (arg) !== "string") {
        throw "answer argument must be a string";
    } 

    var text = arg;
    text = resolveInlineDictionary(resolveVariables(resolveInlineDictionary(text)));
    systemAnswer(text);
    tts(text);
}

function tts(speech) {
    // 1. process substitutions
    speech = resolveInlineDictionary(speech);
    speech = resolveVariables(speech);
    speech = $reactions.template(speech, $jsapi.context());

    var $response = $jsapi.context().response;

    // 2. fill tts in reply
    var lastReply = $response.replies[$response.replies.length - 1];
    lastReply.tts = lastReply.tts ? lastReply.tts + " " + speech : speech;

    // 3. fill total tts in response
    $response.tts = $response.tts ? $response.tts + " " + speech : speech;
}

function resolveInlineDictionary(string) {
    var result = "";
    if (string) {
        result = string.toString().replace(/\{([^{\/]*)(\/([^}\/]*))+?\}/g, inlineDictionaryReplacer);
    }
    return result;
}

function inlineDictionaryReplacer(match, p1, p2, p3, offset, string) {
    var alternatives = match.replace(/\{|\}/g, "").split("/");
    var index = testMode() ? 0 : randomInteger(0, alternatives.length - 1);
    return alternatives[index];
}

function getFullReplyText() {
    var $response = $jsapi.context().response;
    var res = "";
    if ($response.replies && $response.replies.length > 0) {
        for (var i = 0; i < $response.replies.length; i++) {
            if ($response.replies[i].text) {
                if (res != "") {
                    res += " ";
                }
                res += $response.replies[i].text;
            }
        }
    }
    return res;
}

$reactions.answer = answer;
