function requestError(response, status, error) {
    var $session = $jsapi.context().session;
    $reactions.answer("Ой, сервис временно не отвечает. Попробуйте чуть позже.");
    $reactions.transition("/start");
    $session = {};
    $.temp.withoutSecondRequest = true;
    return error;
}

function findPersonSendSms() {
    // Компонуем SOAP запрос
    var data = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
        "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:v1=\"http://www.omninet.de/OtWebSvc/v1\">\r\n" +
            "<soap:Body>\r\n" +
                "<v1:InvokeScript>\r\n" +
                    "<v1:Script name=\"WebSerChatBot\" runAt=\"Client\">\r\n" +
                        "<v1:Parameters>\r\n" +
                            "<v1:StringVal name=\"rfsCode\">" + $.temp.rfsCode + "</v1:StringVal>\r\n" +
                                    // отправляем только номер телефона или clientID
                            "<v1:StringVal name="+ $.temp.availableParameter + ">" + $.temp.parameters + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"channelType\">" + $.request.channelType + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"botId\">" + $.request.botId + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"chatId\">" + $.request.data.chatId + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"event\">sendMessage</v1:StringVal>\r\n" +
                        "</v1:Parameters>\r\n" +
                    "</v1:Script>\r\n" +
                "</v1:InvokeScript>r\n" +
            "</soap:Body>\r\n" +
        "</soap:Envelope>";

    // Выполняем запрос
    var res = $http.post(URL, {
        headers: {
            "Operation": "InvokeScript",
            "Content-Type": "text/xml; charset=UTF-8",
            "SOAPAction": "http://www.omninet.de/OtWebSvc/v1/InvokeScript"
        },
        body: data,
        timeout: 10000
    });
    if (!res.isOk || parseXmlHttpResponse(res.data)["soap:Envelope"]["soap:Body"]["InvokeScriptResponse"]["InvokeScriptResult"]["success"] == false) {
        requestError(res.response, res.status);
    } else {
        var body = parseXmlHttpResponse(res.data);
        var bodyParsed = body["soap:Envelope"]["soap:Body"]["InvokeScriptResponse"]["InvokeScriptResult"]["Parameters"]["StringVal"];
        if (bodyParsed) {
            writePersonInfo(bodyParsed);
        }
        return bodyParsed;
    }
}

function writePersonInfo(res) {
    if (res) {
        for (var i = 0; i < res.length; i++) {
            if (res[i].name === "returnCode" && res[i].content === "OK") {
                $.temp.resIsOk = true;
                for (var j = 0; j < res.length; j++) {
                    if (res[j].name === "personName") {
                        $.client.personName = res[j].content;
                    }
                    if (res[j].name === "personNumber") {
                        $.client.personNumber = res[j].content;
                    }
                    if (res[j].name === "personID") {
                        $.client.personID = res[j].content;
                    }
                    if (res[j].name === "personPhone") {
                        $.client.personPhone = res[j].content;
                    }
                    if (res[j].name === "personCity") {
                        $.client.personCity = res[j].content;
                    }
                    if (res[j].name === "smsCode") {
                        $.session.smsCode = res[j].content;
                    }
                    if (res[j].name === "AddressLength") {
                        $.client.AddressLength = res[j].content;
                    }
                    if (res[j].name === "AvanceDate") {
                        $.session.avanceDate = res[j].content;
                    }
                    if (res[j].name === "SalaryDate") {
                        $.session.salaryDate = res[j].content;
                    }
                    if (res[j].name === "rfsInfoCount") {
                        $.session.infoCount = res[j].content;
                    }
                    for (var l = 1; l < $.client.AddressLength + 1; l++) {
                        if (res[j].name === "AddressNumber" + l) {
                            $.client["AddressNumber" + l] = res[j].content;
                        }
                        if (res[j].name === "AddressText" + l) {
                            $.client["AddressText" + l] = res[j].content.replace(/##/gm, "\n");
                        }
                        if (res[j].name === "AddressCity" + l) {
                            $.client["AddressCity" + l] = res[j].content;
                        }
                    }
                    // обработка поиска сотрудников
                    for (var t = 1; t < $.session.infoCount + 1; t++) {
                        if (res[j].name === "rfsInfo" + t) {
                            $.session["rfsInfo" + t] = res[j].content.split("#");
                        }
                    }
                }
            }
        }
    } else {
        requestError();
    }
}

function createRFS(code) {
    var rfsCode = code || "CREATE RFS";
    // Компонуем SOAP запрос
    var data = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
        "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:v1=\"http://www.omninet.de/OtWebSvc/v1\">\r\n" +
            "<soap:Body>\r\n" +
                "<v1:InvokeScript>\r\n" +
                    "<v1:Script name=\"WebSerChatBot\" runAt=\"Client\">\r\n" +
                        "<v1:Parameters>\r\n" +
                            "<v1:StringVal name=\"rfsCode\">" + rfsCode + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsTemplate\">" + $.session.rfsTemplate + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"personNumber\">" + $.client.personNumber + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsAddress\">" + $.session.rfsAddress + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsPhone\">" + $.session.rfsPhone + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsNote\">" + $.session.rfsNote + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsTargetOrganization\">" + $.session.rfsTargetOrganization + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsYears\">" + $.session.rfsYears + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsCountry\">" + $.session.rfsCountry + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsDuration\">" + $.session.rfsDuration + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsPurpose\">" + $.session.rfsPurpose + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsChildFIO\">" + $.session.rfsChildFIO + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsChildBirthDate\">" + $.session.rfsChildBirthDate + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsRecipientFIO\">" + $.session.rfsRecipientFIO + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsPayerFIO\">" + $.session.rfsPayerFIO + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsStatus\">" + $.session.rfsStatus + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsDialog\">" + $.session.rfsDialog + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"Month\">" + $.session.month + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsClaim\">" + $.session.rfsClaim + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsDialog\">" + $.session.rfsDialog + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"personFIO\">" + $.session.contactInfo + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsFeedback\">" + $.session.feedback + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsRating\">" + $.session.rate + "</v1:StringVal>\r\n" +
                            "<v1:StringVal name=\"rfsNumber\">" + $.session.rfsNumber + "</v1:StringVal>\r\n" +
                        "</v1:Parameters>\r\n" +
                    "</v1:Script>\r\n" +
                "</v1:InvokeScript>r\n" +
            "</soap:Body>\r\n" +
        "</soap:Envelope>";

    // Выполняем запрос
    var res = $http.post(URL, {
        headers: {
            "Operation": "InvokeScript",
            "Content-Type": "text/xml; charset=UTF-8",
            "SOAPAction": "http://www.omninet.de/OtWebSvc/v1/InvokeScript"
        },
        body: data,
        timeout: 10000
    });
    if (!res.isOk || parseXmlHttpResponse(res.data)["soap:Envelope"]["soap:Body"]["InvokeScriptResponse"]["InvokeScriptResult"]["success"] == false) {
        requestError(res.response, res.status);
    } else {
        var body = parseXmlHttpResponse(res.data);
        return body["soap:Envelope"]["soap:Body"]["InvokeScriptResponse"]["InvokeScriptResult"]["Parameters"]["StringVal"];
    }
}

// вывод кнопок при пагинации
function showMoreButtons(currentPage, pageLength) {
    if (currentPage == 0) {
        if (pageLength > 5) {
            $.temp.buttons.push("Ещё ->");
        }
    } else {
        if ((currentPage + 1) >= (pageLength / 5)) {
            $.temp.buttons.push("<- Назад");
        } else {
            $.temp.buttons.push("<- Назад", "Ещё ->");
        }
    }
    $reactions.buttons($.temp.buttons);
}

function showAllAddressesOrCities(cities) {
    $.temp.buttons = [];
    if (cities) {
        if ($.session.currentCitiesPage && $.session.currentCitiesPage < 0) {
            $.session.currentCitiesPage = 0;
        } else if ($.session.currentCitiesPage >= $.session.cities.length / 5) {
            $.session.currentCitiesPage -= 1;
        }
        var start = $.session.currentCitiesPage * 5;
        for (var i = start; i < $.session.cities.length && i < (start + 5); i++) {
            $.temp.buttons.push($.session.cities[i]);
        }
        showMoreButtons($.session.currentCitiesPage, $.session.cities.length);
    } else {
        if ($.session.currentAddressPage && $.session.currentAddressPage < 0) {
            $.session.currentAddressPage = 0;
        } else if ($.session.currentAddressPage >= $.session.Index.length / 5) {
            $.session.currentAddressPage -= 1;
        }
        var start = $.session.currentAddressPage * 5;
        for (var i = start; i < $.session.Index.length && i < (start + 5); i++) {
            $.temp.buttons.push($.client["AddressText" + $.session.Index[i]]);
        }
        showMoreButtons($.session.currentAddressPage, $.session.Index.length);
    }
}

function updatePage(flag, page) {
    $.session[page] = flag == "more" ? $.session[page] + 1 : $.session[page] - 1;
}

function getNameFromParseTree($parseTree) {
    if ($parseTree.lastName) {
        $.temp.lastName = capitalize($parseTree.lastName[0].text);
    } else if ($parseTree.AnyWord && $parseTree.name) {
        $.temp.lastName = capitalize($parseTree.AnyWord[0].text);
    }
    if ($parseTree.name) {
        $.temp.firstName = $parseTree.name[0].value.name;
    } else if ($parseTree.AnyWord) {
        $.temp.firstName = capitalize($parseTree.AnyWord[0].text);
    }
    if ($parseTree.middleName) {
        $.temp.middleName = capitalize($parseTree.middleName[0].text);
    }
}

function getFullName($parseTree) {
    getNameFromParseTree($parseTree);
    return $.temp.lastName + " " + $.temp.firstName + " " + $.temp.middleName;
}

function checkChildMiddleName($parseTree) {
    if ($parseTree.middleName) {
        $.session.rfsChildFIO = getFullName($parseTree);
    } else {
        $.session.childWithoutMiddleName = $.session.childWithoutMiddleName || 0;
        $.session.childWithoutMiddleName += 1;
        if ($.session.childWithoutMiddleName > 1) {
            delete $.session.childWithoutMiddleName;
            getNameFromParseTree($parseTree);
            $.session.rfsChildFIO = $.temp.lastName + " " + $.temp.firstName;
        } else if (!$.temp.oneAnswer) {
            delete $.session.rfsChildFIO;
            $reactions.answer({"value": "Укажите, пожалуйста, Ф.И.О. ребенка полностью", "tts": "Укажите, пожалуйста, имя ребенка полностью"});
            $.temp.oneAnswer = true;
        }
    }
}

function checkDate($parseTree, childCareAllowance) {
    if ($parseTree.date) {
        DateRecognition.getPeriod($parseTree);
    } else {
        DateRecognition.getRelDate($parseTree, true);
    }
    var monthsFromNow = moment($.session.startDate, "DD.MM.YYYY").diff(moment(currentDate()), "months");
    delete $.session.rfsDuration;
    delete $.session.rfsDurationTts;
    if (childCareAllowance) {
        if (monthsFromNow < -24) {
            $reactions.answer("Срок обращения за выплатой пособия по уходу за ребенком – не позднее 6 месяцев со дня достижения ребенком возраста полутора лет.");
            $.temp.oneAnswer = true;
            $reactions.transition("/HowToHelp");
        } else {
            $.session.rfsChildBirthDate = $.session.startDate;
        }
    } else {
        if (monthsFromNow <= -6) {
            $reactions.answer("К сожалению, если ребенок старше полугода, оформить пособие не получится.");
            $.temp.oneAnswer = true;
            $reactions.transition("/HowToHelp");
        } else {
            $.session.rfsChildBirthDate = $.session.startDate;
        }
    }
}

function checkMiddleName($parseTree) {
    if ($parseTree.middleName) {
       return getFullName($parseTree);
    }
    $.session.withoutMiddleName = $.session.withoutMiddleName || 0;
    $.session.withoutMiddleName += 1;
    if ($.session.withoutMiddleName > 1) {
        delete $.session.withoutMiddleName;
        getNameFromParseTree($parseTree);
        return $.temp.lastName + " " + $.temp.firstName;
    }
}

function getSeparateSymbolsTts(propertyName) {
    function fixNumberAnswerTts(match) {
        return match.split("").join(" ");
    }
    return propertyName.replace(/(?!SR-)(\d+)/m, fixNumberAnswerTts);
}

function showMessageFromRequest(code) {
    var res = createRFS(code);
    if (res) {
        for (var i = 0; i < res.length; i++) {
            // при создании заявки на оценку приходит "ОК" кириллицей, в остальных случаях латиницей
            if (res[i].name === "returnCode" && (res[i].content === "OK" || res[i].content === "ОК")) {
                $.temp.resIsOk = true;
                for (var j = 0; j < res.length; j++) {
                    if (res[j].name === "rfsMessage" || (res[j].name === "returnText" && res[j].content)) {
                        $.temp.rfsMessage = res[j].content;
                    }
                }
            }
        }
    }
    if ($.temp.rfsMessage && $.temp.resIsOk) {
        $.temp.tts = getSeparateSymbolsTts($.temp.rfsMessage);
        $reactions.answer({"value": $.temp.rfsMessage, "tts": $.temp.tts});
    } else {
        $reactions.answer("Что-то пошло не так, ваше обращение не было создано, попробуйте позже.");
        $reactions.transition("/HowToHelp");
    }
}

function checkDateAndName($parseTree, childCareAllowance) {
    var path = (childCareAllowance) ? "/ChildCareAllowance/AskForNotific/Yes/GetFullNameAndDateOfBurth" : "/LumpSum/AskForNotific/Yes/GetFullNameAndDateOfBurth";
    if ($parseTree.date || $parseTree.value && $parseTree.value.day && $parseTree.value.month || $parseTree.DateDayMonthYearDigit || $parseTree.DateRelativeDay || $parseTree.DateTimeRelativeWeeks || $parseTree.DateRelativeMonthNew) {
        if ($parseTree.date) {
            DateRecognition.getPeriod($parseTree);
        } else {
            DateRecognition.getRelDate($parseTree, true);
        }
        var monthsFromNow = moment($.session.startDate, "DD.MM.YYYY").diff(moment(currentDate()), "months");
        delete $.session.rfsDuration;
        delete $.session.rfsDurationTts;
        if (childCareAllowance) {
            if (monthsFromNow < -24) {
                $reactions.answer("Срок обращения за выплатой пособия по уходу за ребенком – не позднее 6 месяцев со дня достижения ребенком возраста полутора лет.");
                $.temp.oneAnswer = true;
                $reactions.transition("/HowToHelp");
            } else {
                $.session.rfsChildBirthDate = ($.session.startDate) ? $.session.startDate : false;
            }
        } else {
            if (monthsFromNow <= -6) {
                $reactions.answer("К сожалению, если ребенок старше полугода, оформить пособие не получится.");
                $.temp.oneAnswer = true;
                $reactions.transition("/HowToHelp");
            } else {
                $.session.rfsChildBirthDate = ($.session.startDate) ? $.session.startDate : false;
            }
        }
    }
    if ($parseTree.name || $parseTree.lastName) {
        checkChildMiddleName($parseTree);
    }
    if ($.session.lastState.startsWith(path)) {
        $.session.localRepetition = $.session.localRepetition || 0;
        $.session.localRepetition += 1;
        if ($.session.localRepetition >= 4) {
            $.temp.oneAnswer = true;
            $reactions.transition("/CatchAll");
        }
    }
    if (!$.session.rfsChildBirthDate) {
        if (!$.session.rfsChildFIO && !$.temp.oneAnswer) {
            $reactions.answer({"value": "Уточните, пожалуйста, полное Ф.И.О. и дату рождения ребенка.", "tts": "Уточните, пожалуйста, полное имя и дату рождения ребенка."});
        } else if ($.session.lastState.startsWith(path) && !$.temp.oneAnswer) {
            $reactions.answer("Уточните, пожалуйста, дату рождения ребенка.");
        } else if (!$.temp.oneAnswer) {
            $reactions.answer("Хорошо, теперь уточните, пожалуйста, дату рождения ребенка.");
        }
    } else if (!$.session.rfsChildFIO) {
        if ($.session.lastState.startsWith(path) && !$.session.childWithoutMiddleName && !$.temp.oneAnswer) {
            $reactions.answer({"value": "Уточните, пожалуйста, полное Ф.И.О. ребенка.", "tts": "Уточните, пожалуйста, полное имя ребенка."});
        } else if (!$.session.childWithoutMiddleName && !$.temp.oneAnswer) {
            $reactions.answer({"value": "Хорошо, теперь уточните, пожалуйста, полное Ф.И.О. ребенка.", "tts": "Хорошо, теперь уточните, пожалуйста, полное имя ребенка."});
        }
    } else {
        $reactions.transition(childCareAllowance ? "/SWS/AskForWindow" : "./AskForOrg");
    }
}

function setMarkupMode(mode) {
    var $response = $jsapi.context().response;
    $response.replies = $response.replies || [];
    if ($response.replies) {
        for (var i = 0; i < $response.replies.length; i++) {
            var r = $response.replies[i];
            if (r.type === "text" && !r.markup) {
                r.markup = mode;
            }
        }
    }
}

function checkYearsDuration() {
    if ($.session.rfsYears.toString().length == 4) {
        return "Вас интересует период с января " + $.session.rfsYears + " по декабрь " + $.session.rfsYears + "?";
    }
    return "Вас интересует период с января " + $.session.startYear + " по декабрь " + $.session.endYear + "?";
}

function fullRfsTemplate() {
    if ($.session.rfsTemplate === "2-НДФЛ") {
        return "Заявка на выдачу справки 2-НДФЛ";
    } else if ($.session.rfsTemplate === "Алименты" && $.session.rfsStatus === "Плательщик") {
        return "Заявка на выдачу справки плательщику алиментов";
    } else if ($.session.rfsTemplate === "Алименты" && $.session.rfsStatus === "Получатель") {
        return "Заявка на выдачу справки получателю алиментов";
    } else if ($.session.rfsTemplate === "Средний Заработок") {
        return "Заявка на выдачу справки о среднем заработке за период (по требованию)";
    } else if ($.session.rfsTemplate === "Справка с места работы") {
        return "Заявка на предоставление справки с места работы";
    } else if ($.session.rfsTemplate === "Пособие по уходу за ребенком") {
        return "Заявка на выдачу справки о неполучении пособия по уходу за ребенком до 1,5 лет";
    } else if ($.session.rfsTemplate === "Посольство") {
        return "Заявка на предоставление справки о среднем заработке (в посольство)";
    } else if ($.session.rfsTemplate === "Дополнительные дни отпуска") {
        return "Заявка на предоставление справки о неполучении дополнительных дней отпуска";
    } else if ($.session.rfsTemplate === "Биржа труда") {
        return "Заявка на предоставление справки о среднем заработке (на биржу)";
    } else if ($.session.rfsTemplate === "Пособие единовременное") {
        return "Заявка на выдачу справки о неполучении единовременного пособия по рождению ребенка";
    } else if ($.session.rfsTemplate === "Расчетный счет") {
        return "Заявка на предоставление справки о том, что расчетный счет является зарплатным";
    } else if ($.session.rfsTemplate === "Копия трудовой книжки") {
        return "Заявка на предоставление копии трудовой книжки";
    } else if ($.session.rfsTemplate === "Расчетный лист") {
        return "Заявка на предоставление расчетного листа";
    } else if ($.session.rfsTemplate === "Исполнительный лист") {
        return "Заявка на предоставление копии исполнительного листа";
    }
}

function defaultTransition() {
    if ($.session.rfsTemplate === "2-НДФЛ") {
        $.temp.nextState = "/2_NDFL/AskForDuration";
    } else if ($.session.rfsTemplate === "Алименты") {
        $.temp.nextState = "/Alimony/AskRecipientOrPayer";
    } else if ($.session.rfsTemplate === "Средний Заработок") {
        $.temp.nextState = "/AverageInc/AskForOrg";
    } else if ($.session.rfsTemplate === "Справка с места работы") {
        $.temp.nextState = "/CertFromWorkPlace/AskForOrg";
    } else if ($.session.rfsTemplate === "Пособие по уходу за ребенком") {
        $.temp.nextState = "/ChildCareAllowance/AskForNotific";
    } else if ($.session.rfsTemplate === "Посольство") {
        $.temp.nextState = "/Embassy/AskForCountryAndPeriod";
    } else if ($.session.rfsTemplate === "Дополнительные дни отпуска") {
        $.temp.nextState = "/FurtherMaternityLeave/AskForLastWorkDay";
    } else if ($.session.rfsTemplate === "Копия трудовой книжки") {
        $.temp.nextState = "/JobHistoryCopy/AskForOrg";
    } else if ($.session.rfsTemplate === "Биржа труда") {
        $.temp.nextState = "/LaborExchange/AskForLastWorkDay";
    } else if ($.session.rfsTemplate === "Пособие единовременное") {
        $.temp.nextState = "/LumpSum/AskForNotific";
    } else if ($.session.rfsTemplate === "Расчетный лист") {
        $.temp.nextState = "/PayrollCopy/AskForPeriod";
    } else if ($.session.rfsTemplate === "Исполнительный лист") {
        $.temp.nextState = "/PerfListCopy/AskForDoc";
    } else if ($.session.rfsTemplate === "Расчетный счет") {
        $.temp.nextState = "/SalaryBankAcc/AskForOrg";
    }
    deleteData();
}

function checkTransition($parseTree) {
    if ($parseTree._Root == "Date" || $parseTree._Root == "DateNumber") {
        delete $.session.rfsDuration;
        delete $.session.rfsDurationTts;
        delete $.session.rfsYears;
        delete $.session.rfsYearsText;
        delete $.session.yearsChecked;
        if ($.session.rfsTemplate === "Алименты") {
            $.temp.nextState = "/Alimony/AskRecipientOrPayer/GetAnswer/GetName/GetChildName";
        } else if ($.session.rfsTemplate === "Средний Заработок") {
            delete $.session.rfsPurpose;
            $.temp.nextState = "/AverageInc/AskForOrg/OtherOrg";
        } else if ($.session.rfsTemplate === "Посольство") {
            $.temp.nextState = "/Embassy/AskForCountryAndPeriod/GetDateAndCountry";
        } else if ($.session.rfsTemplate === "Дополнительные дни отпуска" && $parseTree._Root == "Date") {
            $.temp.nextState = "/FurtherMaternityLeave/AskForLastWorkDay/DisabledChild/GetName/GetNumberOfDays";
        } else if ($.session.rfsTemplate === "Дополнительные дни отпуска" && $parseTree._Root == "DateNumber") {
            delete $.session.numberDays;
            $.temp.nextState = "/FurtherMaternityLeave/AskForLastWorkDay/DisabledChild/GetName";
        } else if ($.session.rfsTemplate === "Исполнительный лист") {
            $.temp.nextState = "/PerfListCopy/AskForDoc/GetDoc/GetOrg";
        } else {
            defaultTransition();
        }
    } else if ($parseTree._Root == "FIO" && $.session.rfsTemplate === "Алименты") {
        delete $.session.rfsRecipientFIO;
        delete $.session.rfsPayerFIO;
        $.temp.nextState = "/Alimony/AskRecipientOrPayer/GetAnswer";
    } else if ($parseTree._Root == "ChildFIO") {
        delete $.session.rfsChildFIO;
        if ($.session.rfsTemplate === "Алименты") {
            $.temp.nextState = "/Alimony/AskRecipientOrPayer/GetAnswer/GetName";
        } else if ($.session.rfsTemplate === "Пособие по уходу за ребенком") {
            $.temp.nextState = "/ChildCareAllowance/AskForNotific/Yes/GetFullNameAndDateOfBurth";
        } else if ($.session.rfsTemplate === "Пособие единовременное") {
            $.temp.nextState = "/LumpSum/AskForNotific/Yes/GetFullNameAndDateOfBurth";
        } else {
            defaultTransition();
        }
    } else if ($parseTree._Root == "BirthDate") {
        delete $.session.rfsChildBirthDate;
        if ($.session.rfsTemplate === "Пособие по уходу за ребенком") {
            $.temp.nextState = "/ChildCareAllowance/AskForNotific/Yes/GetFullNameAndDateOfBurth";
        } else if ($.session.rfsTemplate === "Пособие единовременное") {
            $.temp.nextState = "/LumpSum/AskForNotific/Yes/GetFullNameAndDateOfBurth";
        } else if ($.session.rfsTemplate === "Дополнительные дни отпуска") {
            $.temp.nextState = "/FurtherMaternityLeave/AskForLastWorkDay/DisabledChild";
        } else {
            defaultTransition();
        }
    } else if ($parseTree._Root == "Purpose") {
        delete $.session.rfsPurpose;
        if ($.session.rfsTemplate === "Средний Заработок") {
            $.temp.nextState = "/AverageInc/AskForOrg/TUC";
        } else if ($.session.rfsTemplate === "Посольство") {
            $.temp.nextState = "/Embassy/AskForCountryAndPeriod/PurposeOfTrip";
        } else {
            defaultTransition();
        }
    } else if ($parseTree._Root == "Country" && $.session.rfsTemplate === "Посольство") {
        delete $.session.rfsCountry;
        $.temp.nextState = "/Embassy/AskForCountryAndPeriod/GetDateAndCountry";
    } else if ($parseTree._Root == "Organisation") {
        delete $.session.rfsTargetOrganization;
        if ($.session.rfsTemplate === "Алименты") {
            $.temp.nextState = "/Alimony/AskRecipientOrPayer/GetAnswer/GetName/GetChildName";
        } else if ($.session.rfsTemplate === "Копия трудовой книжки") {
            $.temp.nextState = "/JobHistoryCopy/AskForOrg";
        } else if ($.session.rfsTemplate === "Пособие единовременное") {
            $.temp.nextState = "/LumpSum/AskForNotific/Yes/GetFullNameAndDateOfBurth/AskForOrg";
        } else if ($.session.rfsTemplate === "Исполнительный лист") {
            $.temp.nextState = "/PerfListCopy/AskForDoc/GetDoc";
        } else {
            defaultTransition();
        }
    } else if ($parseTree._Root == "Note") {
        delete $.session.rfsNote;
        if ($.session.rfsTemplate === "Посольство") {
            $.temp.nextState = "/Embassy/AskForCountryAndPeriod/AskForRecommendations";
        } else if ($.session.rfsTemplate === "Копия трудовой книжки") {
            $.temp.nextState = "/JobHistoryCopy/AskForOrg/GetOrg";
        } else if ($.session.rfsTemplate === "Исполнительный лист") {
            $.temp.nextState = "/PerfListCopy/AskForDoc";
        } else {
            defaultTransition();
        }
    } else if ($parseTree._Root == "NumberDays" && $.session.rfsTemplate === "Дополнительные дни отпуска") {
        delete $.session.numberDays;
        $.temp.nextState = "/FurtherMaternityLeave/AskForLastWorkDay/DisabledChild/GetName";
    } else {
        defaultTransition();
    }
}
function getMonthTextGenitive(number) {
    var month = moment(number, "M").locale("ru").format("MMMM");
    return $nlp.inflect(month, "gent");
}

function getMonthTextPrepositional(number) {
    var month = moment(number, "M").locale("ru").format("MMMM");
    return $nlp.inflect(month, "loct");
}

function fillSessionCities() {
    var arr = [];
    for (var i = 1; i < $.client.AddressLength + 1; i++) {
        arr.push($.client["AddressCity" + i]);
    }
    $.session.cities = _.uniq(arr);
}
function getMonthsFromNow(period) {
    moment.locale("ru");
    var date = period.replace(/.{10,50}(по|до)./gm, "");
    date = moment(date, "DDMMMMY").format("DD.MM.YYYY");
    $.temp.monthsFromNow = moment(date, "DD.MM.YYYY").diff(moment(currentDate()), "months");
}

function getMonthsText(date) {
    return moment(date, "MM.YYYY").locale("ru").format("MMMM YYYY");
}

function deleteData(all) {
    delete $.session.rfsNote;
    delete $.session.rfsTargetOrganization;
    delete $.session.rfsYears;
    delete $.session.rfsDuration;
    delete $.session.rfsCountry;
    delete $.session.rfsPurpose;
    delete $.session.rfsChildFIO;
    delete $.session.rfsChildBirthDate;
    delete $.session.rfsRecipientFIO;
    delete $.session.rfsPayerFIO;
    delete $.session.rfsStatus;
    delete $.session.numberDays;
    delete $.session.month;
    delete $.session.avanceDate;
    delete $.session.salaryDate;
    delete $.session.rfsClaim;
    delete $.session.rfsTemplate;
    delete $.session.claimNumber;
    delete $.session.contactInfo;
    delete $.session.infoCount;
    delete $.session.rfsDurationTts;
    delete $.session.rfsYearsText;
    delete $.session.numberDaysInPast;
    delete $.session.rate;
    delete $.session.rfsNumber;
    delete $.session.feedback;
    if (all) {
        delete $.session.repetition;
        delete $.session.localRepetition;
        delete $.session.rfsPhone;
        delete $.session.rfsAddress;
        delete $.session.AddressText;
        delete $.session.date;
        delete $.session.feedback;
        delete $.session.chatHistory;
        delete $.session.yearsChecked;
    }
}

function GetPushLink() {
    var pushgateArgs = {
        "channelType": $.request.channelType,
        "botId": $.request.botId,
        "chatId": $.request.data.chatId,
        "event": "eventRating",
        "eventData": {
            "rfsNumber": "3480466"
        }
    };
    var pushgateOptions = {
        dataType: "json",
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Basic anVzdHB1c2hnYXRlOmRjc0dBdThVMjN2WldQUXE="
        },
        body: pushgateArgs
    };
    var pushgateUrl = "http://127.0.0.1:9160/restapi/pushback/";
    $.temp.pushgateResponse = $http.post(pushgateUrl, pushgateOptions);
    $.session.link = $.temp.pushgateResponse.data.link;
}

//================================= функции для "назад" ===========================================


function rememberLastState() {
    $.session.lastState = currentState();
}

function pushBackState() {
    $.session.backState = $.session.backState || [];
    var curState = currentState();
    if ($.session.backState[0] != curState || $.session.backState.length == 0) {
        $.session.backState.unshift(curState);
    }
    if ($.session.backState.length > 10) {
        $.session.backState = $.session.backState.slice(0, 10);
    }
}

function currentState() {
    return $jsapi.context().currentState;
}

function dontPushState() {
    $.session.dontPushState = true;
}

function popBackState() {
    if ($.session.backState) {
        while ($.session.backState.length > 0) {
            var state = $.session.backState.shift();
            if (state != $.session.lastState) {
                if (state.startsWith("/WaitingForRefName")
                    || state.startsWith("/start/MainMenuButtons")
                    || state.startsWith("/Auth") || state.startsWith("/CatchAll")) {
                    state = "/start";
                }
                return state;
            }
        }
    }
    return "/start";
}

function checkButtonTransition() {
    if ($.request.query && !$.request.event) {
        if ($.request.query.toLowerCase() == "назад" || $.request.query.toLowerCase() == "вернуться" 
            || $.request.query.toLowerCase() == "вернись") {
            $.temp.targetState = popBackState();
        }
    }
}

//===================== функции для "главного меню" ===========================================

function mainMenuButton() {
    var $response = $jsapi.context().response;
    $response.replies = $response.replies || [];
    if ($response.replies) {
        for (var i = 0; i < $response.replies.length; i++) {
            var r = $response.replies[i];
            if (r.type === "buttons") {
                r.buttons.text = r.buttons.push({text: "В начало"});
                var buttonAdded = true;
                break;
            }
        }
    }
    if (!buttonAdded) {
        $reactions.buttons({button: {text: "В начало"}});
    }
}

function noMainMenuButton() {
    $.session.noMainMenu = true;
}


    function configureMessage(context, botName, date) {
        var message = [];
        message.push("*Дата: " + date + "*");
        message.push("*Имя бота:* " + botName);
        if (context.request.data && context.request.data.requestHeaders && context.request.data.requestHeaders.referer) {
            message.push("*Окружение:* " + context.request.data.requestHeaders.referer);
        }
        message.push("*Канал:* " + context.request.channelType);
        message.push("*BotId:* " + context.request.channelBotId);
        message.push("*SessionId:* " + context.sessionId);
        message.push("*AccountId:* " + context.request.accountId);
        message.push("*Последний запрос:* " + context.request.query);
        message.push("*Целeвой стейт:* " + context.temp.classifierTargetState);
        message.push("*Тип ошибки:* " + context.exception.type);
        message.push("*Сообщение об ошибке:* " + context.exception.message);
        return message.join("\n\n").replace(/_/gm, "-");
    }
    
    function createJiraIssue(authToken, botName, message) {
        var url = "https://digintel.atlassian.net/rest/api/2/issue";
        var options = {
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Basic " + authToken
            },
            body: {
              "fields": {
                "summary": botName + " Баг",
                "issuetype": {
                  "id": "10024"
                },
                "components": [
                  {
                    "id": "10000"
                  }
                ],
                "project": {
                  "key": "LT"
                },
                "assignee": {
                  "id": "6123598d7a1bfb007164862a"
                },
                "description": message
              }
            }
        }
        return $http.post(url, options);
    }
    function createJiraComment(authToken, key, date) {
        var url = "https://digintel.atlassian.net/rest/api/3/issue/" + key + "/comment";
        var options = {
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Basic " + authToken
            },
            body: {
              "body": {
                "type": "doc",
                "version": 1,
                "content": [
                  {
                    "type": "paragraph",
                    "content": [
                      {
                        "text": "Еще 1 ошибка: " + date ,
                        "type": "text"
                      }
                    ]
                  }
                ]
              }
            }
        }
        return $http.post(url, options);
        
    }
    