require: common.js
  module = zb-common

require: patterns.sc
  module = zb-common

require: number/number.sc
  module = zb-common

require: where/where.sc
  module = zb-common

require: dateTime/dateTime.sc
  module = zb-common

require: namesRu/namesRu.sc
  module = zb-common

require: topics/auth.sc
require: topics/2_NDFL.sc
require: topics/salaryBankAccount.sc
require: topics/averageInc.sc
require: topics/embassy.sc
require: topics/childCareAllowance.sc
require: topics/laborExchange.sc
require: topics/alimony.sc
require: topics/certFromWorkPlace.sc
require: topics/lumpSum.sc
require: topics/payrollCopy.sc
require: topics/furtherMaternityLeave.sc
require: topics/jobHistoryCopy.sc
require: topics/perfListCopy.sc
require: topics/rate.sc
require: topics/SWS.sc
require: topics/allowance.sc
require: topics/taxDeduction.sc
require: topics/vacationSalary.sc
require: topics/faq.sc

require: scripts/funcs.js
require: scripts/getDateForPeriod.js
require: scripts/tts.js

require: patterns.sc

init:

    $global.$ = {
        __noSuchProperty__: function(property) {
            return $jsapi.context()[property];
        }
    };

    $global.Number_To_Call = $injector.numberToCall;
    $global.Number_To_Call2 = $injector.numberToCall2;
    $global.URL = $injector.URL;

    bind("postProcess", function($context) {
        $jsapi.context().session.lastState = $jsapi.context().currentState;
        setMarkupMode("markdown");
        rememberLastState();
        if (!$context.session.dontPushState) {
            pushBackState();
        }
        if (!$context.session.noMainMenu) {
            mainMenuButton();
        }
        if ($jsapi.context().currentState.match(/\/LocalCatchall/gmi)) {
            $.session.localRepetition = $.session.localRepetition || 0;
            $.session.localRepetition += 1;
            if ($.session.localRepetition >= 3) {
                $reactions.transition("/CatchAll");
            }
        }
    });

    bind("preProcess", function($context) {
        var $temp = $context.temp;
        var $request = $context.request;
        var $parseTree = $context.parseTree;
        $.session.newLine = $request.channelType == ("telegram") || $request.channelType == ("zenbox") ? "\n—" : "\n*";
        checkButtonTransition();
        $.session.dontPushState = false;
        $.session.noMainMenu = false;
        if ($parseTree._Root == "reset") {
            $jsapi.startSession();
            if ($parseTree.text.indexOf("start") > -1 && $parseTree.text.length > 6) {
                $.session.start = $parseTree.text.slice(6, $parseTree.text.length);
            }

            if ($.session.start) {
                try {
                    $.session.start = JSON.parse($.session.start);
                } catch (e) {
                    log("start data non JSON object");
                }
            }
        }
    });

    $jsapi.bind({
        type: "preProcess",
        name: "savingVisitorChatHistory",
        path: "/",
        handler: function($context) {
            $context.session.chatHistory = $context.session.chatHistory || [];
            var chatHistory = $context.session.chatHistory;
            if ($context.request.query) {
                chatHistory.push("CLIENT: " + $context.request.query + " &#x000d;&#x000a;");
            }
            chatHistory.splice(0, chatHistory.length - 10);
        }
    });

    $jsapi.bind({
        type: "postProcess",
        name: "savingBotChatHistory",
        path: "/",
        handler: function($context) {
            $context.session.chatHistory = $context.session.chatHistory || [];
            var chatHistory = $context.session.chatHistory;
            if ($context.response.replies) {
                $context.response.replies
                    .filter(function(val) { return val.type === "text"; })
                    .forEach(function(val) { chatHistory.push("BOT: " + val.text + " &#x000d;&#x000a;"); });
            }
            chatHistory.splice(0, chatHistory.length - 10);
        }
    });

theme: /

    state: start
        q!: *start *: reset
        q!: [~главное/в] * (меню/начало/экран*/стоп/отмена) $weight<+1> : reset
        q: [~главное/в] * (меню/начало/экран*/стоп/отмена) $weight<+1> : reset || fromState =/LumpSum/AskForNotific/Yes/GetFullNameAndDateOfBurth/AskForOrg
        q: [~главное/в] * (меню/начало/экран*/стоп/отмена) $weight<+1> : reset || fromState = /Embassy/AskForCountryAndPeriod/AskForRecommendations
        q: [~главное/в] * (меню/начало/экран*/стоп/отмена) $weight<+1> : reset || fromState = /Alimony/AskRecipientOrPayer/GetAnswer/GetName/GetChildName/AskForPeriod
        q: (~главное/в) (меню/начало/экран*) $weight<+1> : reset || fromState = /FAQ/FileClaim/Yes
        q: (стоп/отмена/*start) $weight<+1> : reset || fromState = /FAQ/FileClaim/Yes
        q!: * $hello *
        q: * || fromState = /Auth/AskNumber/GetNumber/CheckNumber/Yes, onlyThisState = true
        q: * $yes *
        q: * $what (ты/вы) (умеешь/можешь/умеете/можете) * 
        q: * помо* *
        q: * (помо*/давай*) * || fromState = /WaitingForRefName, onlyThisState = true 
        if: $parseTree._Root == "reset"
            script: deleteData(true);
        script:
            noMainMenuButton();
        if: $client.auth
            if: (!$client.welcome  && $session.lastState && !$session.lastState.startsWith("/Auth")) || (!$client.welcome && !$session.lastState)
                a: Приветствую вас, {{$injector.name}}
                script: $client.welcome = true;
            a: Я помогу вам заказать справку, копию документа или проконсультировать по некоторым вопросам. Что из этого вас интересует?
            go!: /start/MainMenuButtons
        else:
            a: Здравствуйте! Я цифровая помощница по кадровым задачам «Предприятия №1». Если вы хотите заказать справку или получить кадровую консультацию, то для дальнейшей работы необходимо вас авторизовать.
            go!: /Auth/AskNumber

        state: MainMenuButtons
            buttons:
               "Заказать справку"
               "Заказать копию документа"
               "Задать вопрос"
               "Техподдержка" -> /FAQ/TechSupport
               "Сменить пользователя"

        state: Certificate
            q: * (~справка/справочк*) *
            q: * (справк*/справочк*) $weight<+0.2> * || fromState = /start/Questions, onlyThisState = true
            q: * (справк*/справочк*) $weight<+0.2> * || fromState = /Allowance/ChooseAllowance, onlyThisState = true
            q: * (справк*/справочк*) $weight<+0.2> * || fromState = /Allowance/ChooseAllowance/WhereToApply, onlyThisState = true
            q: * (справк*/справочк*) $weight<+0.2> * || fromState = /TaxDeduction
            q: * (справк*/справочк*) $weight<+0.2> * || fromState = /VacationSalary
            q: * (назад/верни*/вернуть/обратно) * || fromState = /start/Certificate/SecondPage, onlyThisState = true
            q: * (назад/верни*/вернуть/обратно) * || fromState = /WaitingForRefName, onlyThisState = true
            q: * (~справка/справочк*) *:fromWaitingForRefName || fromState = /WaitingForRefName, onlyThisState = true
            if: $parseTree._Root == "fromWaitingForRefName"
                a: Какой документ вас интересует?
            else:
                a: Что вас интересует?
            buttons:
               "2-НДФЛ"
               "Средний заработок"
               "Справка с места работы"
               "Пособие по уходу за ребенком"
               "Биржа труда"
               "Далее"
            go!: /WaitingForRefName

            state: SecondPage
                q: * $next *
                q: * $next * || fromState = /WaitingForRefName, onlyThisState = true
                a: Что вас интересует?
                script: $session.backState[0] = "/start/Certificate";
                buttons:
                   "Алименты"
                   "Пособие по рождению"
                   "Посольство"
                   "Дополнительные дни отпуска"
                   "Расчетный счет"
                   "Назад"
                go!: /WaitingForRefName

        state: Copy
            q: * *копи* *
            q: * *копи* * || fromState = /WaitingForRefName, onlyThisState = true
            a: Копия какого документа вас интересует?
            buttons:
               "Копия трудовой книжки"
               "Исполнительный лист"
               "Расчетный лист"
            go!: /WaitingForRefName

        state: Questions
            q: * (консульт*/вопрос*) *
            q: * (консульт*/вопрос*) * || fromState = /Allowance/ChooseAllowance
            q: * (консульт*/вопрос*) * || fromState = /TaxDeduction
            q: * (консульт*) * || fromState = /VacationSalary
            a: Вы можете задать вопрос по пособиям, уточнить информацию по отпускам, заработной плате или авансу и проблемах с их выплатами. Или узнать больше о налоговых вычетах. Какой вопрос вас интересует?
            buttons:
               "Пособия"
               "Отпуск"
               "Зарплата и Аванс"
               "Налоговые вычеты"
               "К справкам" 

    state: WaitingForRefName

    state: Bye
        q: * {($no/ниче*) * [$thanks]} *
        q: * $bye *
        a: Всего доброго! Обращайтесь ещё!
        go: /start

    state: HowToHelp
        q!: * $thanks *
        a: Могу еще чем-то помочь?
        script:
            deleteData(true);
        go!: /start/MainMenuButtons

    state: CatchAll || noContext = true
        event!: noMatch
        script:
            pushBackState();
            if ($session.lastState && !$session.lastState.startsWith("/CatchAll") && !$session.lastState.startsWith("/WaitingForRefName")
                && !$session.lastState.endsWith("/VacationDaysLeft") && !$session.lastState.endsWith("/СurrentVacation")
                && !$session.lastState.endsWith("/VacationSchedule")) {
                $session.lastMeaningfulState = $session.lastState;
            }

            // Начинаем считать попадания в кэчол с нуля, когда предыдущий стейт не кэчол.
            if ($session.backState && $session.backState.length >= 3 && !$session.backState[2].startsWith("/CatchAll")) {
                $session.repetition = 0;
            } else {
                $session.repetition = $session.repetition || 0;
            }

            // увеличиваем счётчик входов в catchAll
            $session.repetition += 1;
            // условие для стейта Нужна помощь
            $session.repetition = $temp.needHelp ? 3 : $session.repetition;
        if: $session.repetition < 3 && $session.lastState && !$session.lastState.startsWith("/CatchAll") && !($session.localRepetition >= 3) 
            if: $session.lastState.startsWith("/WaitingForRefName")
                a: Документа с похожим названием нет в моём списке. Попробуйте переформулировать вопрос.
                go!: /start
            else:
                random: 
                    a:  Простите, мне не удалось распознать ваш ответ.
                    a:  Простите, я не совсем вас поняла.
                    a:  Переформулируйте, пожалуйста.
                    a:  К сожалению, я вас не поняла.
                    a:  Простите, не совсем понимаю, о чём вы.
                if: $session.lastMeaningfulState
                    go!: {{$session.lastMeaningfulState}}
        else: 
            script: $session.repetition = 0; $session.localRepetition = 0;
            a: Сейчас я затрудняюсь вам помочь. Хотите, чтобы с вами связался специалист?
            buttons:
              "Да"
              "Спасибо, не надо"

        state: Yes
            q: * ($yes/$agree) *
            go!: /HowToHelp

        state: No
            q: * ($no/$disagree) *
            go!: /HowToHelp

    state: NeedHelp
        q!: * не* (~знать/понима*/объясни*/понятн*/ясно)
        q!: * (что * делать/объясни*/поясни*) *
        q!: * (~другой причин*/ничего [$AnyWord] не * (был*/подход*)) *
        script:
            $temp.needHelp = true;
        go!: /CatchAll

    state: reset
        q!: *reset : reset
        q!: * смени* пользователя * : reset
        script:
            $client = {};
            deleteData(true);
        go!: /start  
