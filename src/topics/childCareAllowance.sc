theme: /ChildCareAllowance

    state: AskForNotific
        q: * (уход*/рожд*) [за] ребенк* * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * не * (декрет* [отпуск*]) * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * (уход*/рожд*) [за] ребенк* * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        q: * не * (декрет* [отпуск*]) * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        a: Вы предоставляли документы о рождении ребёнка для внесения данных о ребёнке в систему?
        script:
            $session.rfsTemplate = "Пособие по уходу за ребенком";
        buttons:
            "Да"
            "Нет" 
            
        state: Yes
            q: * ($agree/уведом*/предоставл*) *
            a: {{$injector.name}}, уточните, пожалуйста, полное Ф.И.О. и дату рождения ребенка.
            go: ./GetFullNameAndDateOfBurth

            state: GetFullNameAndDateOfBurth 
                q: * {[($lastName|$AnyWord) * ($name|$AnyWord) * [$middleName]] * ($date|$DateAbsoluteWithoutWeekdays|$DateDayMonthYearDigit|$DateRelativeDay|$DateTimeRelativeWeeks|$DateRelativeMonthNew)} *
                q: * {[($name|$AnyWord) * [$middleName] * ($lastName|$AnyWord)] * ($date|$DateAbsoluteWithoutWeekdays|$DateDayMonthYearDigit|$DateRelativeDay|$DateTimeRelativeWeeks|$DateRelativeMonthNew)} *
                q: * {(($lastName|$AnyWord) * ($name|$AnyWord) * [$middleName]) * [$date|$DateAbsoluteWithoutWeekdays|$DateDayMonthYearDigit|$DateRelativeDay|$DateTimeRelativeWeeks|$DateRelativeMonthNew]} *
                q: * {(($name|$AnyWord) * [$middleName] * ($lastName|$AnyWord)) * [$date|$DateAbsoluteWithoutWeekdays|$DateDayMonthYearDigit|$DateRelativeDay|$DateTimeRelativeWeeks|$DateRelativeMonthNew]} *
                script:
                    checkDateAndName($parseTree, true);

                state: NoMatch
                    q: * ($disagree/не уведом*) * 
                    go!: /CatchAll

        state: No
            q: * ($disagree/не (уведом*/предоставл*)) *
            a: Для оформления справки вам необходимо обратиться с оригиналом свидетельства о рождении ребенка в любое удобное для вас Единое окно для внесения данных в систему о рождении ребенка.
            go!: /HowToHelp
