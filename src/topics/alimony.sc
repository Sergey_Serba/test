theme: /Alimony

    state: AskRecipientOrPayer
        q: * {алимент* * [$recipient]} * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * {алимент* * [$payer]} * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * {алимент* * [$recipient]} * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        q: * {алимент* * [$payer]} * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        script:
            $session.rfsTemplate = "Алименты";
            if ($parseTree.recipient || $parseTree.payer) {
                $reactions.transition("./GetAnswer");
            }
        a: {{$injector.name}}, уточните, пожалуйста, Вы являетесь получателем или плательщиком алиментов?
        buttons:
            "Получатель"
            "Плательщик" 
            "Третье лицо"

        state: Other
            q: * (друго*/трет*/не я) *
            q: * {(ни/не) (получ*/одно/первое/то) * (ни/не) (плат*/второе/другое)} *   
            a: Вы не можете оформить справку за другого человека.
            go!: /HowToHelp
            
        state: GetAnswer
            q: * $recipient *
            q: * $payer *
            script:
                if ($parseTree.recipient || $session.rfsStatus === "Получатель") {
                    $reactions.answer({"value": "Уточните, пожалуйста, полное Ф.И.О. того, кто платит алименты.", "tts": "Уточните, пожалуйста, полное имя того, кто платит алименты."});
                    $session.rfsStatus = "Получатель";
                } else if ($parseTree.payer || $session.rfsStatus === "Плательщик") {
                    $reactions.answer({"value": "Уточните, пожалуйста, полное Ф.И.О. того, кто получает алименты.", "tts": "Уточните, пожалуйста, полное имя того, кто получает алименты."});
                    $session.rfsStatus = "Плательщик";
                }

            state: GetName
                q: * ($lastName|$AnyWord) * ($name|$AnyWord) * [$middleName] *
                q: * ($name|$AnyWord) * [$middleName] * ($lastName|$AnyWord) *
                script:
                    if ($parseTree.name || $parseTree.lastName) {
                        if ($session.rfsStatus === "Получатель") {
                            $session.rfsPayerFIO = checkMiddleName($parseTree);
                        } else {
                            $session.rfsRecipientFIO = checkMiddleName($parseTree);
                        }
                    }
                if: $session.rfsChildFIO
                    go!: ./GetChildName
                elseif: !$session.rfsPayerFIO && !$session.rfsRecipientFIO
                    a: Укажите, пожалуйста, Ф.И.О. полностью. || tts = "Уточните, пожалуйста, имя полностью."
                    go: /Alimony/AskRecipientOrPayer/GetAnswer
                else:
                    a: Уточните, пожалуйста, полное Ф.И.О. ребенка. || tts = "Уточните, пожалуйста, полное имя ребенка."
                
                state: GetChildName
                    q: * ($lastName|$AnyWord) * ($name|$AnyWord) * [$middleName] *
                    q: * ($name|$AnyWord) * [$middleName] * ($lastName|$AnyWord)  *
                    script:
                        if ($parseTree.name || $parseTree.lastName) {
                            checkChildMiddleName($parseTree);
                        }
                    if: $session.rfsChildFIO && !$session.rfsDuration && !$session.rfsYears
                        a: Уточните, пожалуйста, за какой период нужна справка? Например, май 2020 – сентябрь 2020. || tts = "Уточните, пожалуйста, за какой период нужна справка? Например, май две тысячи двадцатого – сентябрь две тысячи двадцатого."
                    elseif: $session.rfsChildFIO && ($session.rfsDuration || $session.rfsYears)
                        go!: ./AskForPeriod

                    state: AskForPeriod || modal=true
                        q: * ($DateMonthAndDigits [$DateYearNumeric|$DateYearNumber|$DateYearTwoNumber|$DateYearNumberTts]) * [$DateMonthAndDigits [$DateYearNumeric|$DateYearNumber|$DateYearTwoNumber|$DateYearNumberTts]] * :1
                        q: * ($DateYearNumeric|$DateYearNumber|$DateRelativeYear) * [$DateYearNumeric|$DateYearNumber|$DateRelativeYear|$DateYearNumberTts] * :3
                        if: $parseTree._Root == 1
                            script:
                                DateRecognition.getMonthsAndYears($parseTree);
                            if: $temp.endYear == $temp.thisYear && $temp.thisMonth - $temp.endMonth <= 0 || $temp.startYear == $temp.thisYear && $temp.thisMonth - $temp.startMonth <= 0 || $temp.endYear > $temp.thisYear || $temp.thisYear - $temp.startYear < 0
                                a: В период справки не может входить текущий месяц.
                                a: Уточните период, за который вам нужна справка.
                                script: delete $session.rfsDuration;
                            else:
                                if: !$session.rfsTargetOrganization 
                                    a: В какую организацию?
                                    go: ./AskForOrg
                                else:
                                    go!: /SWS/AskForWindow
                        elseif: $parseTree._Root == 3
                            script:
                                DateRecognition.getYears($parseTree);
                            if: $session.rfsYears && !$session.yearsChecked
                                a: {{checkYearsDuration()}}
                                buttons:
                                    "Да"
                                    "Нет"
                            elseif: $session.rfsYears
                                go!: /SWS/AskForWindow
                            else:
                                go: /Alimony/AskRecipientOrPayer/GetAnswer/GetName/GetChildName
                        else: 
                            if: !$session.rfsTargetOrganization 
                                a: В какую организацию?
                                go: ./AskForOrg
                            else:
                                go!: /SWS/AskForWindow

                        state: Yes
                            q: * ($yes/$agree) *
                            script:
                                $session.yearsChecked = true;
                            if: $session.rfsYears.toString().length == 4
                                script:
                                    $session.rfsYearsText = "январь " + $session.rfsYears + " - декабрь " + $session.rfsYears;
                                    $session.rfsYears = "01." + $session.rfsYears + " - 12." + $session.rfsYears;
                            if: !$session.rfsTargetOrganization 
                                a: В какую организацию?
                                go: /Alimony/AskRecipientOrPayer/GetAnswer/GetName/GetChildName/AskForPeriod/AskForOrg
                            else:
                                go!: /SWS/AskForWindow

                        state: No
                            q: * ($no/$disagree) *
                            script: delete $session.rfsYears;
                            go!: /Alimony/AskRecipientOrPayer/GetAnswer/GetName/GetChildName

                        state: AskForOrg
                            q: * $Text * 
                            script:
                                $session.rfsTargetOrganization = $parseTree.Text[0].value;
                            go!: /SWS/AskForWindow
                            
                    state: LocalCatchAll
                        event: noMatch
                        if: !$temp.endDateBeforeStart
                            a: Простите, не удалось распознать период. Попробуйте, пожалуйста, еще раз. 
                        else:
                            a: Вы указали дату окончания раньше, чем дату начала.
