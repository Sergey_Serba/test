theme: /SWS

    state: AskForWindow
        script:
            $session.AddressText = "Москва, Якиманский пер., д. 6, стр. 1";
        #a: Выдача готовых документов производится по адресу: {{$session.AddressText}}
        go!: /SWS/AskForWindow/GetPhoneNumber/AskPhoneNumber

        state: GetPhoneNumber
            
            state: AskPhoneNumber
                if: $session.rfsPhone
                    go!: /SWS/CheckData
                a: Сообщите, пожалуйста, по какому номеру с вами связаться при необходимости уточнения информации?
                buttons:
                    "{{$client.personPhone}}" 
                    "Другой номер"
                go: ./PhoneNumber
                    
                state: PhoneNumber
                    q: * $mobilePhoneNumber $weight<+0.2> *
                    q: * (текущ*/этот/этому/дан*) *:2
                    if: $session.rfsPhone
                        script: delete $session.rfsPhone;
                        go!: /SWS/AskForWindow/GetPhoneNumber/AskPhoneNumber
                    script:
                        if ($parseTree._Root == 2) {
                            $session.rfsPhone = $client.personPhone;
                        } else if ($parseTree.mobilePhoneNumber) {
                            $session.rfsPhone = $parseTree.mobilePhoneNumber[0].value;
                        } else {
                            delete $session.rfsPhone;
                            $reactions.transition("/SWS/AskForWindow/GetPhoneNumber/AskPhoneNumber");
                        }
                    go!: /SWS/CheckData

                state: OtherNumber
                    q: * (друго*/второй/личн*/мобильн*/рабоч*/не (*то*/такой)) *
                    a: Введите, пожалуйста, номер телефона, по которому сообщить вам о готовности справки.
                    go: ../PhoneNumber

                state: CheckNumber
                    q: $phoneNumberdigits $weight<+0.1>
                    script:
                        $session.rfsPhone = $parseTree._phoneNumberdigits;
                        if (!$session.rfsPhone) {
                            $reactions.transition("/SWS/AskForWindow/GetPhoneNumber/AskPhoneNumber");
                        }
                    a: Перепроверьте, пожалуйста, этот номер указан верно: {{$session.rfsPhone}}?
                    buttons:
                        "Да"
                        "Нет"

                    state: Yes
                        q: * ($yes/$agree) *
                        go!: /SWS/CheckData

                    state: No
                        q: * ($no/неправ*/неверн*/некоррект*/$disagree/не* (прав*/верн*/коррект*)) *
                        q: * друго* *
                        script: delete $session.rfsPhone;
                        go!: /SWS/AskForWindow/GetPhoneNumber/AskPhoneNumber

    state: CheckData
        a: Для регистрации обращения подтвердите информацию.
        a: Документ: {{fullRfsTemplate()}};
        if: $session.rfsRecipientFIO 
            a: Ф.И.О. получателя: {{$session.rfsRecipientFIO}}; || tts = "Имя получателя: {{$session.rfsRecipientFIO}}."
        if: $session.rfsPayerFIO 
            a: Ф.И.О. плательщика: {{$session.rfsPayerFIO}}; || tts = "Имя плательщика: {{$session.rfsPayerFIO}}."
        if: $session.rfsChildFIO
            a: Ф.И.О. ребенка: {{$session.rfsChildFIO}}; || tts = "Имя ребенка: {{$session.rfsChildFIO}}."
        if: $session.rfsChildBirthDate
            a: Дата рождения ребенка: {{$session.rfsChildBirthDate}};
        if: $session.rfsPurpose
            a: Цель: {{$session.rfsPurpose}};
        if: $session.rfsDuration && !$session.rfsDurationTts && $session.rfsTemplate !== "Дополнительные дни отпуска"
            a: Период: {{$session.rfsDuration}};
        if: $session.rfsDuration && $session.rfsDurationTts && $session.rfsTemplate !== "Дополнительные дни отпуска"
            a: Период: {{$session.rfsDurationTts}}; 
        if: $session.rfsCountry
            a: Страна: {{$session.rfsCountry}};
        if: $session.rfsYears && !$session.rfsYearsText
            a: Период: {{$session.rfsYears}}; || tts = "Период: {{getYearsTts($session.rfsYears)}}."
        if: $session.rfsYears && $session.rfsYearsText
            a: Период: {{$session.rfsYearsText}}; || tts = "Период: {{getYearsTts($session.rfsYearsText)}}."
        if: $session.rfsTargetOrganization
            a: Организация: {{$session.rfsTargetOrganization}};
        if: $session.rfsNote
            a: Примечание: {{$session.rfsNote}}; || tts =  "Примечание: {{getYearsTts($session.rfsNote)}}"
        a: Ваш контактный номер телефона: {{$session.rfsPhone}}; || tts = "Ваш контактный номер телефона: {{readPhone($session.rfsPhone)}}"
        a: Всё верно?
        buttons:
            "Да"
            "Нет"

        state: WrongInfo
            q: * ($no/неправ*/неверн*/некоррект*/$disagree/не* (прав*/верн*/коррект*)) *
            a: Что следует поправить?
            buttons:
                "Номер телефона"
                #"Адрес СЕО"
                "Информацию по документу"
                "Отмена регистрации"

        state: PhoneNumber
            q: * (номер*/телефон*) *
            a: Укажите, пожалуйста, верный номер телефона.
            script: delete $session.rfsPhone;
            go!: /SWS/AskForWindow/GetPhoneNumber/AskPhoneNumber

        #state: SWS
        #    q: * (СЕО/окн*/адрес*) * 
        #    script:
        #        $temp.chooseSWS = true;
        #        delete $session.rfsAddress;
        #    go!: /SWS/AskForWindow
            
        state: Decline
            q: * отмен* *
            q: * (не [надо/нужно/хочу/буду]/передумал*) * (регистрир*/регистрац*) *
            a: Хорошо, регистрация обращения отменена.
            go!: /HowToHelp

        state: Doc
            q: * документ* *
            if: $session.rfsTemplate === "Расчетный лист" || $session.rfsTemplate === "Копия трудовой книжки" || $session.rfsTemplate === "Исполнительный лист"
                script:
                    $temp.targetState = "/start/Copy";
            else:
                script:
                    $temp.targetState = "/start/Certificate";
            script:
                deleteData(true);
                $reactions.transition($temp.targetState);

        state: Info
            q: * {(инфо*/данные*) * [документ*]} *
            q: * (дат*) * :Date
            q: * (период*) * :DateNumber
            q: * {колич* дн*} * :NumberDays
            q: * (фамили*/имя/отчество/фио) *:FIO
            q: * (фамили*/имя/отчество/фио) * ребенк* *:ChildFIO
            q: * (дат*/день) рожд* *:BirthDate
            q: * (цель/цели) *:Purpose
            q: * стран* *:Country
            q: * (организац*/мест*) *:Organisation
            q: * (примеч*/заметк*/рекомендаци*) *:Note
            script:
                checkTransition($parseTree);
                $reactions.transition($temp.nextState);

        state: SendRequest
            q: * ($yes/$agree) *
            a: Благодарю, ваше обращение зарегистрировано.
            go!: /HowToHelp
