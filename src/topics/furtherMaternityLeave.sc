theme: /FurtherMaternityLeave

    state: AskForLastWorkDay
        q: * доп* * отпуск* * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * ребен* инвали* * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * $one сентябр* * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * доп* * отпуск* * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        q: * ребен* инвали* * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        q: * $one сентябр* * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        a: Вас интересует справка о неполучении дополнительных дней отпуска по уходу за ребенком инвалидом или на 1 сентября?
        script:
            $session.rfsTemplate = "Дополнительные дни отпуска";
        buttons:
            "По уходу за ребенком инвалидом"
            "На 1 сентября"

        state: DisabledChild
            q: * ребен* инвали* *
            q: * уход* ~ребенок *
            a: {{$injector.name}}, уточните, пожалуйста, Ф.И.О. ребенка.

            state: GetName
                q: * ($lastName|$AnyWord) * ($name|$AnyWord) * [$middleName] *
                q: * ($name|$AnyWord) * [$middleName] * ($lastName|$AnyWord) *
                script:
                    if ($parseTree.name || $parseTree.lastName) {
                        checkChildMiddleName($parseTree);
                    }
                if: $session.rfsChildFIO && $session.rfsDuration && $session.numberDays
                    go!: /SWS/AskForWindow
                elseif: $session.rfsChildFIO
                    a: Сколько дней дополнительного отпуска будете брать?
                    buttons:
                        "Не будем брать"
                        "Не брали в прошлом"
                
                state: GetNumberOfDays
                    q: * $Number *
                    q: * (все/сколько можно/макс*) *:4
                    q: * (не [$AnyWord] (буд*/хочу/хочет*/нужен/нужн*/брать)/нискольк*) *:0
                    q: * {(не [$AnyWord] брал*) [в (прошлом/прошедш*)/давн*]} *:past
                    if: $session.numberDays == undefined  
                        if: $parseTree._Root == 4 || $parseTree._Root == 0 
                            script:
                                $session.numberDays = $parseTree._Root;
                        elseif: $parseTree._Root == "past"
                            script:
                                $session.numberDays = 0;
                                $session.numberDaysInPast = true;
                        elseif: $parseTree.Number && $parseTree.Number[0].value < 5 && $parseTree.Number[0].value > -1
                            script:
                                $session.numberDays = $parseTree.Number[0].value.toString();
                        else: 
                            a: Вы можете взять максимум 4 дня или не брать вообще. Скажите, пожалуйста, сколько дней дополнительного отпуска будете брать?
                            go: /FurtherMaternityLeave/AskForLastWorkDay/DisabledChild/GetName
                    if: ($session.numberDays || $session.numberDays == "0") && !$session.rfsDuration 
                        a: Когда? Укажите год и месяц.
                    else: 
                        go!: ./GetPeriod

                    state: GetPeriod
                        q: * ($DateMonthAndDigits [$DateYearNumeric|$DateYearNumber|$DateYearTwoNumber|$DateYearNumberTts] * [$DateMonthAndDigits [$DateYearNumeric|$DateYearNumber|$DateYearTwoNumber|$DateYearNumberTts]]) *
                        script:
                            if (!$session.rfsDuration) {
                                $temp.oneDate = true;
                                DateRecognition.getMonthsAndYears($parseTree);
                                var monthsFromNow = moment($session.rfsDuration, "MM.YYYY").diff(moment(currentDate()), "months");
                                if (($session.numberDaysInPast && monthsFromNow >= 0) || (!$session.numberDaysInPast && monthsFromNow < 0)) {
                                    if ($session.numberDaysInPast) {
                                        $reactions.answer("Если вы не брали в прошлом дополнительные дни, то год и месяц может быть только в прошлом. Давайте ещё раз уточним.");
                                    } else {
                                        $reactions.answer("Если вы не будете брать дополнительные дни, то год и месяц может быть только в будущем. Давайте ещё раз уточним.");
                                    }
                                    delete $session.rfsDuration;
                                    delete $session.numberDays;
                                    delete $session.numberDaysInPast;
                                    $reactions.transition("/FurtherMaternityLeave/AskForLastWorkDay/DisabledChild/GetName");
                                }
                            }
                            if ($session.rfsDuration) {
                                $session.rfsNote = "\nВид справки: по уходу за ребенком инвалидом.";
                                if ($session.numberDays == "0" && !$session.numberDaysInPast) {
                                    $session.rfsNote = $session.rfsNote + "\nСотрудник не будет брать дополнительные дни в " + getMonthTextPrepositional($session.rfsDuration.substr(0, 2)) + " " + $session.rfsDuration.substr(3) + " года";
                                } else if ($session.numberDays == "0" && $session.numberDaysInPast) {
                                    $session.rfsNote = $session.rfsNote + "\nСотрудник не брал дополнительные дни в " + getMonthTextPrepositional($session.rfsDuration.substr(0, 2)) + " " + $session.rfsDuration.substr(3) + " года";
                                } else {
                                    $session.rfsNote = $session.rfsNote + "\nСотрудник будет брать " + $session.numberDays + " " + $nlp.conform("день", $session.numberDays) + " в " + getMonthTextPrepositional($session.rfsDuration) + " " + $session.rfsDuration.substr(3) + " года";
                                }
                            }
                        go!: /SWS/AskForWindow 

                    state: LocalCatchAll
                        event: noMatch
                        if: !$temp.endDateBeforeStart
                            a: Простите, не удалось распознать дату. Попробуйте, пожалуйста, еще раз. Например, май 2019.
                        else:
                            a: Вы указали дату окончания раньше, чем дату начала.

        state: FirstSept
            q: * $one сентябр* *
            script:
                $session.rfsNote = "Сотрудник не будет брать доп. день на 1 сентября";
            go!: /SWS/AskForWindow 