theme: /CertFromWorkPlace

    state: AskForOrg
        q: * мест* работ* * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * ((где/должност*) * работ*/должност*) * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * мест* работ* * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        q: * ((где/должност*) * работ*/должност*) * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        a: {{$injector.name}}, в какую организацию вам необходима справка? Выберите из предложенных или введите свой вариант.
        script:
            $session.rfsTemplate = "Справка с места работы";
        buttons:
            "Суд"
            "Полиция"
            "Соцзащита" 
            "Детский сад/Школа" 
            
        state: GetOrg
            q: * $Text *
            script:
                $session.rfsTargetOrganization = $parseTree.Text[0].value;
            go!: /SWS/AskForWindow