theme: /LaborExchange

    state: AskForLastWorkDay
        q: * (бирж* [труд*]/центр* занятост*) * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * (бирж* [труд*]/центр* занятост*) * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        a: {{$injector.name}}, уточните, пожалуйста, у вас сегодня последний рабочий день?
        script:
            $session.rfsTemplate = "Биржа труда";
        buttons:
            "Да"
            "Сегодня не последний" 

        state: Today
            q: * (уволен*/$yes/$agree/последний) *
            q: * ($DateAbsoluteWithoutWeekdays|$DateDayMonthYearDigit|$DateRelativeDay|$DateTimeRelativeWeeks|$DateRelativeMonthNew) *:2
            script:
                if ($parseTree._Root == 2) {
                    DateRecognition.getRelDate($parseTree);
                    var daysFromNow = moment($session.date, "DD.MM.YYYY").diff(moment({hours: 0}), "days");
                    if (!moment($session.date, "DD.MM.YYYY").isValid()) {
                        $reactions.transition("../LocalCatchAll");
                    }
                    if (daysFromNow > 0) {
                        $reactions.transition("/LaborExchange/AskForLastWorkDay/NotToday");
                    }
                }
            go!: /SWS/AskForWindow

        state: LocalCatchAll
            event: noMatch
            a: Простите, не удалось распознать период. Попробуйте, пожалуйста, еще раз. 
                
        state: NotToday
            q: * (не последний/предпоследний) *
            q: * $no *
            a: {{$injector.name}}, для подготовки справки на Биржу труда вам необходимо обратиться в последний рабочий день или позвонить по номеру {{Number_To_Call}} или {{Number_To_Call2}} после увольнения.
            go!: /HowToHelp
    