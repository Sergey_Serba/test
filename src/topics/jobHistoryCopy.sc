theme: /JobHistoryCopy

    state: AskForOrg
        q: * (*копи*/дубликат*) труд* [кни*] * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * (*копи*/дубликат*) труд* [кни*] * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        q: * (трудовой/трудовая) * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * (трудовой/трудовая) * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        script:
            $session.rfsTemplate = "Копия трудовой книжки";
        a: {{$injector.name}}, уточните, пожалуйста, куда требуется копия документа.
 
        state: GetOrg
            q: * $Text *
            script:
                $session.rfsTargetOrganization = $session.rfsTargetOrganization || $parseTree.text;
            if: $session.rfsNote
                go!: /SWS/AskForWindow
            else:
                a: Уточните, пожалуйста, количество экземпляров.
                buttons:
                    "1"
                    "2"
                    "3"

            state: GetNumberOfCopies
                q: * $Number *
                script:
                    if ($parseTree.Number[0].value < 1 || $parseTree.Number[0].value > 10) {
                        $reactions.answer("Вы можете заказать не более 10 экземпляров за раз.");
                        $reactions.transition("/JobHistoryCopy/AskForOrg/GetOrg");
                    } else {
                        $session.rfsNote = "Количество экземпляров - " + $parseTree.Number[0].value;
                    }
                go!: /SWS/AskForWindow
