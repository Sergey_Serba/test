theme: /AverageInc

    state: AskForOrg
        q: * $salary * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * (соц* защит*/профком*/проф* комитет*/требовани*) * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * $salary * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        q: * (соц* защит*/профком*/проф* комитет*/требовани*) * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true

        q: * (верни*/назад/верну*/отмен*) * :return || fromState = /AverageInc/AskForOrg/OtherOrg, onlyThisState = true
        if: $parseTree._Root === "return"
            script: delete $session.rfsTargetOrganization;
        a: Куда требуется справка о среднем заработке?
        script:
            $session.rfsTemplate = "Средний Заработок";
        buttons:
            "Профком"
            "Другая организация"
            
        state: TUC
            q: * (профком*/проф* комитет*) *
            q: * (ссуд*/кредит*/заем*/займ*/$three [месяц*]) * :1
            q: * (лагер*/двенадцат* [месяц*]/год*) * :2
            if: $session.rfsTargetOrganization === "Другая организация"
                go!: /AverageInc/AskForOrg/OtherOrg/LocalCatchAll
            script:
                $session.rfsTargetOrganization = "Профком";
                if ($parseTree._Root == 2 || $parseTree._Root == 1) {
                    $reactions.transition("./Purpose");
                }
            a: Для чего вам необходима справка?
            buttons:
                "Для ссуды (за 3 мес.)"
                "Для путевки в лагерь (за 12 мес.)"
            
            state: Purpose
                q: * (ссуд*/$three [месяц*]) * :1
                q: * (лагерь/двенадцат*/год*) * :2
                script:
                    if ($parseTree._Root === 1) {
                        $session.rfsPurpose = "Ссуда";
                        $session.rfsDuration = moment().subtract("months", 3).format("DD.MM.YYYY")
                        + " - " + moment().format("DD.MM.YYYY");
                    } else if ($parseTree._Root === 2) {
                        $session.rfsPurpose = "Лагерь";
                        $session.rfsDuration = moment().subtract("years", 1).format("DD.MM.YYYY")
                        + " - " + moment().format("DD.MM.YYYY");
                    }
                go!: /SWS/AskForWindow
                
        state: OtherOrg
            q: * (~другой/~иной) *
            q: * (не * профком*) $weight<+0.1> *
            script:
                $session.rfsTargetOrganization = "Другая организация";
            a: За какой период вам нужна справка? Например, июнь 2020 - сентябрь 2020.
            
            state: Period
                q: * ($DateMonthAndDigits [$DateYearNumeric|$DateYearNumber|$DateYearTwoNumber|$DateRelativeYear|$DateYearNumberTts]) * [$DateMonthAndDigits [$DateYearNumeric|$DateYearNumber|$DateYearTwoNumber|$DateRelativeYear|$DateYearNumberTts]] *:1
                q: * ($DateYearNumeric|$DateYearNumber|$DateYearTwoNumber|$DateRelativeYear|$DateYearNumberTts) * [$DateYearNumeric|$DateYearNumber|$DateYearTwoNumber|$DateRelativeYear|$DateYearNumberTts] * :3
                if: $parseTree._Root === 1
                    script:
                        DateRecognition.getMonthsAndYears($parseTree);
                    if: $temp.endYear === $temp.thisYear && $temp.thisMonth - $temp.endMonth <= 0 || $temp.startYear == $temp.thisYear && $temp.thisMonth - $temp.startMonth <= 0 || $temp.endYear > $temp.thisYear || $temp.thisYear - $temp.startYear < 0
                        a: В период справки не может входить текущий или будущий месяц.
                        a: Уточните период, за который вам нужна справка.
                        script: delete $session.rfsDuration;
                    else: 
                        go!: /SWS/AskForWindow
                elseif: $parseTree._Root === 3
                    script:
                        DateRecognition.getYears($parseTree);
                        $temp.thisYear = new Date().getFullYear();
                    if: $session.rfsYears === $temp.thisYear || $session.rfsYears > $temp.thisYear 
                        a: В период справки не может входить текущий или будущий месяц.
                        a: Уточните период, за который вам нужна справка.
                        script: delete $session.rfsYears;
                if: $session.rfsDuration 
                    go!: /SWS/AskForWindow
                elseif: $session.rfsYears
                    a: {{checkYearsDuration()}}
                    buttons:
                        "Да"
                        "Нет"

                state: Yes
                    q: * ($yes/$agree) *
                    if: $session.rfsYears.toString().length === 4
                        script:
                            $session.rfsYearsText = "январь " + $session.rfsYears + " - декабрь " + $session.rfsYears;
                            $session.rfsYears = "01." + $session.rfsYears + " - 12." + $session.rfsYears;
                    go!: /SWS/AskForWindow

                state: No
                    q: * ($no/$disagree) *
                    script: delete $session.rfsYears;
                    go!: /AverageInc/AskForOrg/OtherOrg
                    
            state: LocalCatchAll
                event: noMatch
                if: !$temp.endDateBeforeStart
                    a: Простите, не удалось распознать период. Попробуйте, пожалуйста, еще раз. 
                else:
                    a: Вы указали дату окончания раньше, чем дату начала.