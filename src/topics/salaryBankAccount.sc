theme: /SalaryBankAcc

    state: AskForOrg
        q: * {(расчетн* счет*) [зарплат*]} * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * {(расчетн* счет*) [зарплат*]} * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        a: Куда вам необходима справка?
        script:
            $session.rfsTemplate = "Расчетный счет";

        state: GetOrg
            q: * $Text *
            script:
                $session.rfsNote = $parseTree.Text[0].value;
            go!: /SWS/AskForWindow 
