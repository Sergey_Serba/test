theme: /Rate 

    state: RateUs
        q!: Check rate 
        event!: eventRating
        script:
            if (!testMode()) {
                $session.rfsNumber = $request.rawRequest.eventData && $request.rawRequest.eventData.rfsNumber ? $request.rawRequest.eventData.rfsNumber : undefined;
            } else {
                $session.rfsNumber = 3480466;
            }
        a: Пожалуйста, оцените качество предоставленной услуги.
        buttons: 
           "Плохо"
           "Удовлетворительно"
           "Хорошо"
           "Отлично"

        state: GetNumber
            q: * ($bad/не удовлетвори*) *:2
            q: * удовлетворительн* *:3
            q: * хорош* *:4
            q: * (отличн*|классн*|крут*|замечательн*|потряс*|великолепн*|прекрасн*|заебись|зашибись|здорово|здорого|порядок|в порядке*) *:5
            script:
                $session.rate = $parseTree._Root;
            if: $session.rate == 2 
                a: Подскажите, что вас не устроило?
            elseif: $session.rate > 2
                a: Спасибо за обратную связь!
                go!: /HowToHelp

            state: GetComment
                q: * $Text *
                a: Спасибо за обратную связь!
                go!: /HowToHelp

        state: NoService
            q: * {(не*/ничего) [$AnyWord] оказан*} *
            q: * {не (было/*пользовал*) [$AnyWord] услуг*} *
            go!: /HowToHelp
  
        state: LocalCatchAll
            event: noMatch
            random:
                a:  Простите, мне не удалось распознать ваш ответ.
                a:  Простите, я не совсем вас поняла.
                a:  Переформулируйте, пожалуйста.
                a:  К сожалению, я вас не поняла.
                a:  Простите, не совсем понимаю, о чём вы.
