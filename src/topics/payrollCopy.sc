theme: /PayrollCopy

    state: AskForPeriod
        q: * расчет* лист* * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * {кореш* * $salary} * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * расчет* лист* * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        q: * {кореш* * $salary} * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        script:
            $session.rfsTemplate = "Расчетный лист";
        a: {{$injector.name}}, уточните, пожалуйста, за какой месяц вам необходим расчетный лист? Например, май 2019.

        state: GetPeriod
            q: * ($DateMonthAndDigits [$DateYearNumeric|$DateYearNumber|$DateRelativeYear|$DateYearNumberTts] * [$DateMonthAndDigits [$DateYearNumeric|$DateYearNumber|$DateRelativeYear|$DateYearNumberTts]]) *
            if: $parseTree.DateMonthAndDigits && Object.keys($parseTree.DateMonthAndDigits).length === 2
                a: Мы можем предоставить вам только один расчетный лист за раз. За какой месяц и год вам необходима информация (например, май 2019)?
            else:  
                script:
                    $temp.oneDate = true;
                    DateRecognition.getMonthsAndYears($parseTree);
                if: $temp.startYear == $temp.thisYear && $temp.thisMonth - $temp.startMonth <= 0
                    a: В период справки не может входить текущий или будущий месяц.
                    script: delete $session.rfsDuration;
                else:
                    go!: /SWS/AskForWindow 

        state: LocalCatchAll
            event: noMatch
            if: !$temp.endDateBeforeStart
                a: Простите, не удалось распознать дату. Попробуйте, пожалуйста, еще раз. Например, май 2019.
            else:
                a: Вы указали дату окончания раньше, чем дату начала.
