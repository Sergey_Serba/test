theme: /2_NDFL

    state: AskForDuration
        q: * [$two] * (НДФЛ*/налог* * доход* * физ*) * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * [$two] * (НДФЛ*/налог* * доход* * физ*) * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        a: Уточните, пожалуйста, за какие года требуется справка. Например, 2018 - 2019. || tts = "Уточните, пожалуйста, за какие года требуется справка. Например, две тысячи восемнадцатый - две тысячи девятнадцатый."
        script:
            $session.rfsTemplate = "2-НДФЛ";

        state: GetDuration
            q: * ($DateYearNumeric|$DateYearNumber|$DateRelativeYear|$DateYearNumberTts|$DateYearTwoNumber) * [$DateYearNumeric|$DateYearNumber|$DateRelativeYear|$DateYearNumberTts|$DateYearTwoNumber] *
            q: * (этот/текущий/нынешний) * :2
            script:
                $temp.onlyYears = true;
                DateRecognition.getYears($parseTree);
                var thisYear = new Date().getFullYear();
                if (thisYear - $session.rfsYears >= 5) {
                    delete $session.rfsYears;
                    $reactions.answer("Период справки не должен включать текущий месяц, а давность не должна превышать 4 года.");
                }
            if: $session.rfsYears 
                go!: /SWS/AskForWindow

        state: LocalCatchAll
            event: noMatch
            if: !$temp.endDateBeforeStart
                a: Простите, не удалось распознать период. Попробуйте, пожалуйста, еще раз. 
            else:
                a: Вы указали дату окончания раньше, чем дату начала.