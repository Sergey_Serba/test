theme: /Embassy

    state: AskForCountryAndPeriod
        q: * посольств* * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * виз* * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * посольств* * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        q: * виз* * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        script:
            $session.rfsTemplate = "Посольство";
        a: Для получения справки в посольство назовите, пожалуйста, страну, которую планируете посетить, и период пребывания.
            
        state: GetDateAndCountry
            q: * {[$Country] * ($date * [$date])} *
            q: * {[$Country] * ([с] $date [по/до] $date)}  * 
            # q: * {[$Country] * ([с] $Number [по/до] $Number)} *  
            q: * {[$Country] * ([с] $Number [по/до] $date)} *
            
            q: * {$Country * [$date * [$date]]}  *
            q: * {$Country * [[с] $date [по/до] $date]}  * 
            # q: * {$Country * [[с] $Number [по/до] $Number]} *  
            q: * {$Country * [[с] $Number [по/до] $date]} *

            q: * {[$Country] * ($DateMonthAndDigits [$DateYearNumeric|$DateYearNumber|$DateRelativeYear|$DateYearNumberTts|($DateYearTwoNumber $weight<-0.3>)]) * [$DateMonthAndDigits [$DateYearNumeric|$DateYearNumber|$DateRelativeYear|$DateYearNumberTts|($DateYearTwoNumber $weight<-0.2>)]] $weight<+0.5>} *
            q: * {$Country * [$DateMonthAndDigits [$DateYearNumeric|$DateYearNumber|$DateRelativeYear|$DateYearNumberTts|($DateYearTwoNumber $weight<-0.3>)]] * [$DateMonthAndDigits [$DateYearNumeric|$DateYearNumber|$DateRelativeYear|$DateYearNumberTts|($DateYearTwoNumber $weight<-0.2>)]] $weight<+0.5>} *

            q: * [$Country] ($DateYearNumeric|$DateYearNumber|$DateRelativeYear|$DateYearNumberTts|($DateYearTwoNumber $weight<-0.3>)) * [$DateYearNumeric|$DateYearNumber|$DateRelativeYear|$DateYearNumberTts|($DateYearTwoNumber $weight<-0.2>)] $weight<+0.5> * :3
            q: * $Country [$DateYearNumeric|$DateYearNumber|$DateRelativeYear|$DateYearNumberTts|($DateYearTwoNumber $weight<-0.3>)] * [$DateYearNumeric|$DateYearNumber|$DateRelativeYear|$DateYearNumberTts|($DateYearTwoNumber $weight<-0.2>)] $weight<+0.5> * :3
            script:
                if ($parseTree.date || $parseTree.Number) {
                    $temp.shouldBeFuture = true;
                    DateRecognition.getPeriod($parseTree);
                } else if ($parseTree.DateMonthAndDigits) {
                    DateRecognition.getMonthsAndYears($parseTree);
                } else if ($parseTree._Root == 3 && ($parseTree.DateYearNumeric || $parseTree.DateYearNumber || $parseTree.DateRelativeYear)) {
                    $temp.couldBeFuture = true;
                    DateRecognition.getYears($parseTree);
                }
                if ($parseTree.Country) {
                    $session.rfsCountry = $parseTree.Country[0].value.name;
                }
                if ($temp.oneAnswer) {
                    $reactions.transition("../LocalCatchAll");
                } else if (!$session.rfsCountry) {
                    $reactions.answer("Укажите, пожалуйста, страну пребывания.");
                } else if (!$session.rfsDuration && !$session.rfsYears) {
                    $reactions.answer("Укажите, пожалуйста, период пребывания.");
                } else if ($session.rfsYears && $session.rfsCountry && !$session.yearsChecked) {
                    $reactions.answer(checkYearsDuration());
                    $reactions.buttons([{text: "Да"}, {text: "Нет"}]);
                } else {
                    $reactions.transition("/Embassy/AskForCountryAndPeriod/PurposeOfTrip");
                }

            state: Yes
                q: * ($yes/$agree) *
                if: $session.rfsYears.toString().length == 4
                    script:
                        $session.rfsYearsText = "январь " + $session.rfsYears + " - декабрь " + $session.rfsYears;
                        $session.rfsYears = "01." + $session.rfsYears + " - 12." + $session.rfsYears;
                script:
                    $session.yearsChecked = true;
                go!: /Embassy/AskForCountryAndPeriod/PurposeOfTrip

            state: No
                q: * ($no/$disagree) *
                script: delete $session.rfsYears;
                go!: /Embassy/AskForCountryAndPeriod/GetDateAndCountry

        state: LocalCatchAll
            if: !$temp.endDateBeforeStart
                a: Простите, не удалось распознать период. Попробуйте, пожалуйста, еще раз. 
            else:
                a: Вы указали дату окончания раньше, чем дату начала.

        state: PurposeOfTrip
            if: $session.rfsPurpose
                go!: /Embassy/AskForCountryAndPeriod/AskForRecommendations
            else:
                a: Уточните, пожалуйста, цель поездки.
                buttons:
                    "Командировка"
                    "Туризм"
                    "Другое"
        
            state: GetPurpose
                q: * $Text *
                script:
                    $session.rfsPurpose = $parseTree.Text[0].value;
                go!: /Embassy/AskForCountryAndPeriod/AskForRecommendations

            state: OtherPurpose
                q: * (~другой/ничего/не * (это/то/знаю/знать/представл*/поним*)) *
                a: Напишите цель поездки.

        state: AskForRecommendations || modal=true
            if: $session.rfsNote
                go!: /SWS/AskForWindow
            else:
                a: Есть ли у вас дополнительные рекомендации от посольства? Пожалуйста, сообщите.
                buttons:
                    "Нет рекомендаций"

            state: GetRecommendations    
                q: * $Text *
                script:
                    $session.rfsNote = $parseTree.text;
                go!: /SWS/AskForWindow

            state: NoRecommendations
                q: * ($no/нету/отсутству*) * [рекомендаци*] *
                script:
                    $session.rfsNote = "Нет рекомендаций";
                go!: /SWS/AskForWindow