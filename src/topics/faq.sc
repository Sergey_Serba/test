theme: /FAQ

    state: TopicChoice
        q: часто задаваемые вопросы
        q: * faq
        go!: ../TechSupport
        a: Возможно, Вас интересует что-то из предложенного.
        buttons:
            "Техническая проблема" -> /FAQ/TechSupport
            "Табельный номер" -> /FAQ/PersonNumber
            "Бланки заявлений" -> /FAQ/Forms
            "Звонок в СЕО" -> /FAQ/OneWindow
            "Ещё" -> /FAQ/TopicChoice/TopicChoiceSecondPage

        state: TopicChoiceSecondPage
            q: * $next *
            q: * далее *
            a: Что вас интересует?
            buttons:
                "Не могу дозвониться" -> /FAQ/CannotGetThrough
                "Номер телефона коллеги" -> /FAQ/FindContactInfo
                "Дата рождения коллеги" -> /FAQ/FindBirthdayInfo
                "Назад" -> /FAQ/TopicChoice

            state: Yes
                q: $yes
                q: ([~я] интерес* *)
                go!: /FAQ/TopicChoice

            state: No
                q: $no
                q: (* не интерес* *)
                a: На данный момент это все доступные вопросы в разделе F.A.Q. Также я могу проконсультировать вас по пособиям, помочь оформить справку или получить копию документа. Что из этого вас интересует?
                go!: /start/MainMenuButtons

    state: TechSupport
        q: * {[не] [мог*/мож*/получ*/выход*] (*работа*/настро*/настра*/подключ*) [$AnyWord] [$AnyWord] ((программ* [обеспеч*])/систем*/по/сап*/sap/док*/doc*/печат*/принтер*/скан*/монитор*)} * 
        q: * {[не/переста*] (установ*/настро*/*мен*/*работа*) [вычислительн*] (*тех*/компьютер*/мыш*/клав*)} *
        q: * {(забы*/сброс*/*мен*/вспомн*) [$AnyWord] (парол*)} *
        q: * {[заправ*/обнов*/*полн*/*конч*] (картридж*/картредж*/чернил*)} *
        q: * {[не] [мог*/мож*/получ*/выход*] (войти/вход*) в * (систем*/комп*/ноут*)} *
        q: * {(удаленн* доступ*)} *
        q: * {[не] [мог*/мож*/получ*/выход*/настро*] (*печат*/скан*)} *
        q: * {все *лома*} *
        q: * {[все/ничего] не *работ*} *
        q: * {нуж* помо* [$AnyWord] (тех*/комп*/телефон*/монитор*/принтер*/скан*)} *
        q: * {тех* (проблем*/вопрос*)}  * 
        q: * {[не] [мог*/мож*/получ*/выход*] (*работа*/настро*/настра*/подключ*) [$AnyWord] [$AnyWord] ((программ* [обеспеч*])/систем*/по/сап*/sap/док*/doc*/печат*/принтер*/скан*/монитор*)} * || fromState = /start
        q: * {[не/переста*] (установ*/настро*/*мен*/*работа*) [вычислительн*] (*тех*/компьютер*/мыш*/клав*)} * || fromState = /start
        q: * {(забы*/сброс*/*мен*/вспомн*) [$AnyWord] (парол*)} * || fromState = /start
        q: * {[заправ*/обнов*/*полн*/*конч*] (картридж*/картредж*/чернил*)} * || fromState = /start
        q: * {[не] [мог*/мож*/получ*/выход*] (войти/вход*) в * (систем*/комп*/ноут*)} * || fromState = /start
        q: * {(удаленн* доступ*)} * || fromState = /start
        q: * {[не] [мог*/мож*/получ*/выход*/настро*] (*печат*/скан*)} * || fromState = /start
        q: * {все *лома*} * || fromState = /start
        q: * {[все/ничего] не *работ*} * || fromState = /start
        q: * {нуж* помо* [$AnyWord] (тех*/комп*/телефон*/монитор*/принтер*/скан*)} * || fromState = /start
        q: * {тех* (проблем*/вопрос*)}  * || fromState = /start
        a: Я могу зарегистрировать обращение в техническую поддержку, и с вами свяжется специалист для уточнения деталей запроса.
        script:
            $session.claimNumber = "TechSupport";
            $reactions.transition("/FAQ/FileClaim");

    state: PersonNumber
        q: * {[у] (~мой/меня/мя) ~табельный ~номер} *
        q: * {[~узнать] ~мой ~номер} *
        q: * {[~узнать]  ~мой * ~табельный ~номер } *
        q: * {[~узнать]  ~мой * ~табельный ~номер } * || fromState = /start
        q: * {[у] (~мой/меня/мя) ~табельный ~номер} * || fromState = /start
        q: * {[~узнать] ~мой ~номер} * || fromState = /start
        a: Ваш табельный номер  Интеграция с бд выключена. Номер клиента согласно предоставленной бд.
        go!: /HowToHelp

    state: Forms
        q: * {[найти/взять/скачать/получить/распечатать] [~бланк] (~заявление)} *
        q: * {[найти/взять/скачать/получить/распечатать] [~бланк] (~заявление)} * || fromState = /start
        a: Бланки заявлений можно самостоятельно скачать на портале «Окей, Предприятие №1» или получить любой бланк заявления в Службе единого окна.
        go!: /HowToHelp

    state: OneWindow
        q: * {(*звон*/связа*/обрати*/обращ*/написа*) [обращени*/заявк*] [в/для] [служб*/специалист*] [$AnyWord] [$oneWindow]} *
        q: $oneWindow
        q: * {(*звон*/связа*/обрати*/обращ*/написа*) [обращени*/заявк*] [в/для] [служб*/специалист*] [$AnyWord] [$oneWindow]} * || fromState = /start
        q: $oneWindow || fromState = /start
        a: Я могу зарегистрировать обращение для специалистов Службы единого окна.
        script:
            $session.claimNumber = 4444;
            $reactions.transition("/FAQ/FileClaim");

    state: CannotGetThrough
        q: * {не [~мочь] [$AnyWord] (~дозвониться/~дозваниваться/позвон*) [до/в] [$AnyWord] [$hr/$support/$Number/$oneWindow]} *
        q: * {не [~мочь] [$AnyWord] (~дозвониться/~дозваниваться/позвон*) [до/в] [$AnyWord] [$hr/$support/$Number/$oneWindow]} * || fromState = /start
        script:
            if ($parseTree.hr || $parseTree._Number === 4444) {
                $reactions.transition("/FAQ/CannotGetThrough/4444");
            } else if ($parseTree.support || $parseTree._Number === 1111) {
                $reactions.transition("/FAQ/CannotGetThrough/1111");
            } else if ($parseTree.onewindow) {
                $reactions.transition("/FAQ/OneWindow");
            } else {
                $reactions.answer({"value": "Уточните, куда вы не можете дозвониться. До «4444» или до «1111»?", "tts": "Уточните, куда вы не можете дозвониться. До сорок четыре сорок четыре или до одиннадцать одиннадцать?"});
                $reactions.buttons([{text: "1111", transition: "/FAQ/CannotGetThrough/1111"}, {text: "4444", transition: "/FAQ/CannotGetThrough/4444"}]);
            }

        state: 1111
            q: * 1111 *
            q: * ($support) *
            q: * {не [~мочь] [$AnyWord] (~дозвониться/~дозваниваться) [до/в] [$AnyWord] ($support)} *
            q: * (четыре единиц*) *
            q: * (одиннадцать одиннадцать) *
            script:
                $session.claimNumber = 1111;
                $reactions.answer("Я могу предложить вам зарегистрировать обращение для специалистов, и они с вами самостоятельно свяжутся.");
                $reactions.transition("/FAQ/FileClaim");

        state: 4444
            q: * 4444 *
            q: * ($hr) *
            q: * {не [~мочь] [$AnyWord] (~дозвониться/~дозваниваться) [до/в] [$AnyWord] ($hr)} *
            q: * (четыре четвер*) *
            q: * (сорок четыре сорок четыре) *
            script:
                $session.claimNumber = 4444;
                $reactions.answer("Я могу предложить вам зарегистрировать обращение для специалистов, и они с вами самостоятельно свяжутся.");
                $reactions.transition("/FAQ/FileClaim");

        state: OneWindow
            q: * $oneWindow *
            script:
                $reactions.transition("/FAQ/OneWindow");

    state: FindContactInfo
        q: * {(телефон*) (номер/телефон*) * [$colleague]} *
        q: * {(телефон*) (номер/телефон*)  * [$colleague]} * || fromState = /start
        a: Для получения этой информации, пожалуйста, напишите Ф.И.О. коллеги. || tts = "Для получения этой информации, пожалуйста, напишите полное имя коллеги."
        go!: /FAQ/FindContactInfo/Name

        state: Name
            buttons:
                "Я не знаю" -> /FAQ/FindContactInfo/Name/NoFullName

            state: FullName
                q: * ($lastName|$AnyWord) * ($name|$AnyWord) * [$middleName] *
                q: * ($name|$AnyWord) * [$middleName] * ($lastName|$AnyWord) *
                q: * {(телефон*) (номер/телефон*)  * [$colleague]} * ($lastName|$AnyWord) * ($name|$AnyWord) * [$middleName] * || fromState = /start 
                q: * {(телефон*) (номер/телефон*)  * [$colleague]} * ($name|$AnyWord) * [$middleName] * ($lastName|$AnyWord) * || fromState = /start 
                a: Поиск телефона сотрудника по его ФИО. Выключено
                go!: /HowToHelp

            state: NoFullName
                q: * ($dontKnow) * || fromState = /FAQ/FindContactInfo
                q: * {[я] [никак/совсем] не [$quite] [$AnyWord] (*помн*/~курс)} * || fromState = /FAQ/FindContactInfo
                q: * (забы*) * || fromState = /FAQ/FindContactInfo
                a:  Без полного Ф.И.О. я не могу помочь узнать эту информацию. || tts = "Без полного имени я не могу помочь узнать эту информацию."
                go!: /HowToHelp

            state: LocalCatchAll
                event: noMatch
                a: Простите, не поняла.
                go!: /FAQ/FindContactInfo


    state: FindBirthdayInfo
        q: * {(~найти/~нужен/~надо/~искать) (((~день/~дата) ~рождение)/др) [$colleague]} *
        q: * {[когда] (((~день/~дата) ~рождение)/др) [у] [$colleague]} *
        q: * {(~найти/~нужен/~надо/~искать) (((~день/~дата) ~рождение)/др) [$colleague]} * || fromState = /start
        q: * {[когда] (((~день/~дата) ~рождение)/др) [у] [$colleague]} * || fromState = /start
        a: Для получения этой информации, пожалуйста, напишите Ф.И.О. коллеги. || tts = "Для получения этой информации, пожалуйста, напишите полное имя коллеги."
        go!: /FAQ/FindBirthdayInfo/Name

        state: Name
            buttons:
                "Я не знаю" -> /FAQ/FindBirthdayInfo/Name/NoFullName

            state: FullName
                q: * ($lastName|$AnyWord) * ($name|$AnyWord) * [$middleName] *
                q: * ($name|$AnyWord) * [$middleName] * ($lastName|$AnyWord) *
                q: * {[когда] (((~день/~дата) ~рождение)/др) [у] [$colleague]} * ($lastName|$AnyWord) * ($name|$AnyWord) * [$middleName] * || fromState = /start
                q: * {[когда] (((~день/~дата) ~рождение)/др) [у] [$colleague]} * ($name|$AnyWord) * [$middleName] * ($lastName|$AnyWord) * || fromState = /start
                a: Поиск дня рождения сотрудника по его ФИО. Выключено 
                go!: /HowToHelp


            state: NoFullName
                q: * $dontKnow *
                q: * {[я] [никак/совсем] не [$quite] [$AnyWord] (*помн*/~курс)} *
                q: * (забы*) *
                a: Без полного Ф.И.О. я не могу помочь узнать эту информацию. || tts = "Без полного имени я не могу помочь узнать эту информацию."
                go!: /HowToHelp

            state: LocalCatchAll
                event: noMatch
                a: Простите, не поняла.
                go!: /FAQ/FindBirthdayInfo

    state: FileClaim
        a: Хотите, чтобы я зарегистрировала ваше обращение?
        buttons:
            "Да" -> /FAQ/FileClaim/Yes
            "Нет" -> /FAQ/FileClaim/No

        state: Yes || modal = true
            q: * ([ну] мож*) *
            q: {[да/ага] *регистрируй*} *
            q: * {пусть свяж* [специалист]} *
            q: * {хочу чтобы [со мной] связал*} *
            q: * $agree *
            a: Пожалуйста, сформулируйте ваше обращение.
            
            state: Claim
                q: *
                script:
                    $session.rfsClaim = $parseTree.text;
                    $session.rfsPhone = $client.personPhone;
                    $reactions.answer("Сейчас выключено. Создается обращение, аналогично следующему сообщению с любой нужной информацией");
                    $reactions.answer("CALL OPERATOR " + $session.claimNumber + '\n\n' + "Текст обращения - \n" + $session.rfsClaim);
                go!: /HowToHelp


        state: No
            q: * (не (хочу/надо))
            q: (нет [спасибо] $weight<+0.2>) *
            q: * $disagree *
            a: Хорошо, я вас поняла.
            go!: /HowToHelp

        state: LocalCatchAll
            event: noMatch
            a: Простите, не поняла. Мне зарегистрировать обращение по вашему вопросу, чтобы с вами связался специалист?
            go!: /FAQ/FileClaim

    state: CatchAll
        event!: noMatch
        go!: /start/CatchAll
