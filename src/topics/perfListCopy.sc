theme: /PerfListCopy

    state: AskForDoc
        q: * исполни* лист* * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * исполни* лист* * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        script: $session.rfsTemplate = "Исполнительный лист";
        a: {{$injector.name}}, напишите, пожалуйста, какой вид документа вас интересует?
        buttons: 
            "Копия исполнит. листа"
            "Справка о сумме выплат"
            "Справка о сумме задолженности по исполнит. листам"
            "Другое"

        state: OtherDoc
            q: * (не * (знаю/знать/представл*/поним*)) *
            q: * (~другой/ничего/никак*/не * (интерес*/это/то)) *
            a: Эта информация должна быть в обращении.
            go!: /PerfListCopy/AskForDoc

        state: CheckDocType
            q: $Text
            script: $session.rfsNote = $parseTree.text;
            a: В примечании указать вид документа “{{$session.rfsNote}}”, верно?
            buttons:
                "Да"
                "Нет"

            state: Yes
                q: * ($yes/$agree/укажи*/указывай*) *
                go!: /PerfListCopy/AskForDoc/GetDoc

            state: No
                q: * ($no/$disagree) *
                script: delete $session.rfsNote;
                go!: /PerfListCopy/AskForDoc/OtherDoc

        state: GetDoc
            q: * (исполнит*/{сумм* (выплат/задолж*)}) *
            if: !$session.rfsNote
                script:
                    $session.rfsNote = $parseTree.text;
            if: $session.rfsTargetOrganization
                go!: ./GetOrg
            else:
                a: Уточните, пожалуйста, куда требуется копия документа.

            state: GetOrg
                q: * $Text *
                if: !$session.rfsTargetOrganization
                    script:
                        $session.rfsTargetOrganization = $parseTree.text;
                if: $session.rfsDuration || $session.rfsYears
                    go!: /SWS/AskForWindow 
                else:
                    a: {{$injector.name}}, уточните, пожалуйста, за какой период требуется копия документа? Например, май 2020 - июнь 2020.
        
                state: AskForPeriod
                    q: * ($DateMonthAndDigits [$DateYearNumeric|$DateYearNumber|$DateYearTwoNumber|$DateRelativeYear|$DateYearNumberTts]) * [$DateMonthAndDigits [$DateYearNumeric|$DateYearNumber|$DateYearTwoNumber|$DateRelativeYear|$DateYearNumberTts]] *:1
                    q: * ($DateYearNumeric|$DateYearNumber|$DateYearTwoNumber|$DateRelativeYear|$DateYearNumberTts) * [$DateYearNumeric|$DateYearNumber|$DateYearTwoNumber|$DateRelativeYear|$DateYearNumberTts] * :3
                    if: $parseTree._Root == 1
                        script:
                            DateRecognition.getMonthsAndYears($parseTree);
                        if: $temp.endYear == $temp.thisYear && $temp.thisMonth - $temp.endMonth <= 0 || $temp.startYear == $temp.thisYear && $temp.thisMonth - $temp.startMonth <= 0 || $temp.endYear > $temp.thisYear || $temp.thisYear - $temp.startYear < 0
                            a: В период документа не может входить текущий или будущий месяц.
                            a: За какой период вам нужна копия документа?
                            script: delete $session.rfsDuration;
                        else:
                            go!: /SWS/AskForWindow
                    elseif: $parseTree._Root == 3
                        script:
                            DateRecognition.getYears($parseTree);
                    if: $session.rfsDuration 
                        go!: /SWS/AskForWindow 
                    elseif: $session.rfsYears 
                        a: {{checkYearsDuration()}}
                        buttons:
                            "Да"
                            "Нет"

                    state: Yes
                        q: * ($yes/$agree) *
                        if: $session.rfsYears.toString().length == 4
                            script:
                                $session.rfsYearsText = "январь " + $session.rfsYears + " - декабрь " + $session.rfsYears;
                                $session.rfsYears = "01." + $session.rfsYears + " - 12." + $session.rfsYears;
                        go!: /SWS/AskForWindow 

                    state: No
                        q: * ($no/$disagree) *
                        script: delete $session.rfsYears;
                        go!: /PerfListCopy/AskForDoc/GetDoc/GetOrg

                state: LocalCatchAll
                    event: noMatch
                    if: !$temp.endDateBeforeStart
                        a: Простите, не удалось распознать период. Попробуйте, пожалуйста, еще раз. 
                    else:
                        a: Вы указали дату окончания раньше, чем дату начала.