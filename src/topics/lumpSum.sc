theme: /LumpSum

    state: AskForNotific
        q: * {(единовременн*/разов*) пособи*} * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * (пособи*/выплат*) * рожд* * $weight<+0.2> || fromState = /WaitingForRefName, onlyThisState = true
        q: * {(единовременн*/разов*) пособи*} * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        q: * (пособи*/выплат*) * рожд* * $weight<+0.2> || fromState = /start/MainMenuButtons, onlyThisState = true
        a: {{$injector.name}}, вы предоставляли документы о рождении ребёнка для внесения данных о ребёнке в систему?
        script:
            $session.rfsTemplate = "Пособие единовременное";
        buttons:
            "Да"
            "Нет" 

        state: Yes
            q: * ($agree/предоставлял*) *
            a: {{$injector.name}}, уточните, пожалуйста, полное Ф.И.О. и дату рождения ребенка.

            state: GetFullNameAndDateOfBurth
                q: * {[($lastName|$AnyWord) * ($name|$AnyWord) * [$middleName]] * ($date|$DateAbsoluteWithoutWeekdays|$DateDayMonthYearDigit|$DateRelativeDay|$DateTimeRelativeWeeks|$DateRelativeMonthNew)} *
                q: * {[($name|$AnyWord) * [$middleName] * ($lastName|$AnyWord)] * ($date|$DateAbsoluteWithoutWeekdays|$DateDayMonthYearDigit|$DateRelativeDay|$DateTimeRelativeWeeks|$DateRelativeMonthNew)} *
                q: * {(($lastName|$AnyWord) * ($name|$AnyWord) * [$middleName]) * [$date|$DateAbsoluteWithoutWeekdays|$DateDayMonthYearDigit|$DateRelativeDay|$DateTimeRelativeWeeks|$DateRelativeMonthNew]} *
                q: * {(($name|$AnyWord) * [$middleName] * ($lastName|$AnyWord)) * [$date|$DateAbsoluteWithoutWeekdays|$DateDayMonthYearDigit|$DateRelativeDay|$DateTimeRelativeWeeks|$DateRelativeMonthNew]} *
                script:
                    checkDateAndName($parseTree);

                state: AskForOrg || modal = true
                    if: $session.rfsTargetOrganization
                        go!: /SWS/AskForWindow
                    else:
                        a: Уточните, пожалуйста, куда требуется справка.
                    
                    state: GetOrg 
                        q: * $Text *
                        script:
                            $session.rfsTargetOrganization = $parseTree.Text[0].value;
                        go!: /SWS/AskForWindow
                
        state: No
            q: * ($disagree/не предоставлял*) *
            a: Для оформления справки вам необходимо обратиться с оригиналом свидетельства о рождении ребенка в любое удобное для вас Единое окно для внесения данных в систему о рождении ребенка.
            go!: /HowToHelp
