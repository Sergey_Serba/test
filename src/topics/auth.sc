theme: /Auth 

    state: AskNumber
        q: * {(измени*/поменяй*/друг*/перепутал*/неверн*/смен*/исправ*/поправ*) * номер*} * || fromState = /Auth/AskNumber/GetNumber/CheckNumber/SendCode, onlyThisState = true 
        q: * {(ошибся/ошиблась/опечатал*/неправильн*/не правильн*) * [номер*]} * || fromState = /Auth/AskNumber/GetNumber/CheckNumber/SendCode, onlyThisState = true 
        a: Сообщите мне свой номер телефона.

        state: GetNumber
            q: * $mobilePhoneNumber *
            q: * $mobilePhoneNumber $weight<+0.2> * || fromState = /Auth/AskNumber/GetNumber/CheckNumber/SendCode, onlyThisState = true 
            q: * не $Number а $mobilePhoneNumber * || fromState = /Auth/AskNumber/GetNumber/CheckNumber/SendCode, onlyThisState = true 
            script:
                $session.phoneNumber = $parseTree.mobilePhoneNumber[0].value;
                $session.rfsPhone = $session.phoneNumber;
                $client.personPhone = $session.phoneNumber;

            a: Спасибо!
            go!: ./SendCode

            state: SendCode
                script:
                    $client.lastSmsCode = $jsapi.currentTime();
                    $session.smsCode = parseInt($reactions.random(10).toString() + $reactions.random(10).toString() + $reactions.random(10).toString() + $reactions.random(10).toString());
                    if (!$temp.NotNeedAnswer) {
                        $reactions.answer("Вам сейчас должна прийти смс с кодом. Напишите его сюда.");
                    }
                a: Отправка СМС в данный момент выключена, верный код - {{$session.smsCode}}
                buttons:
                    "Отправить повторно"
                    "Изменить телефон" -> /Auth/AskNumber

                state: SendCodeOneMoreTime
                    q: * {(повтор*/еще раз/снова/заново) * (отправ*/пришли*/прислать/присылать/вышли*/выслать)} *
                    q: * {(не (пришел/приход*/придет/пришло)) [код*]} *
                    q: * {(правильн*/верн*) * [код*]} *
                    script:
                        var currentTime = $jsapi.currentTime();
                        var diff = currentTime - $client.lastSmsCode;
                        if ((diff > 30 * 1000) || !$client.lastSmsCode) {
                            $reactions.answer("Итак, отправляю код на номер " + $session.phoneNumber
                                + ". Проверьте, нет ли в номере ошибок. Если есть, сообщите мне, я подкорректирую. Если всё верно, вам должен прийти код. Пришлите мне его.");
                            $temp.NotNeedAnswer = true;
                            $reactions.transition("/Auth/AskNumber/GetNumber/CheckNumber/SendCode");
                        } else {
                            $reactions.answer("Прошло менее минуты. Возможно, смс вот-вот придет. Если пришло, отправьте мне его, пожалуйста.");
                        }

                state: GetSmsCode
                    q: $Number
                    script:
                        $temp.code = $parseTree.Number[0].value.toString().length === 3 ? "0" + $parseTree.Number[0].value : $parseTree.Number[0].value;
                    if: $temp.code == $session.smsCode
                        a: {{$injector.name}}, вы успешно авторизованы!
                        script:
                            delete $session.smsCode;
                            $client.auth = true;
                        go!: /start
                    else: 
                        a: Этот не подошел. Перепроверьте, пожалуйста, и введите заново.
                        buttons:
                            "Отправить повторно"
                            "Изменить телефон" -> /Auth/AskNumber

                state: LocalCatchAll
                    event: noMatch
                    a: Простите, сейчас я ожидаю код. Без него дальнейшая работа невозможна.
                    buttons:
                        "Отправить повторно"
                        "Изменить телефон" -> /Auth/AskNumber

        state: NotANumber
            q: * $Number *
            a: Хм, что-то не похоже на номер. Номер должен начинаться на +79 и состоять из 11 цифр. Пожалуйста, перепроверьте и напишите свой номер телефона.
            go: /Auth/AskNumber
        
        state: LocalCatchAll
            event: noMatch
            a: Простите, не совсем понимаю, о чем вы. Для продолжения процесса авторизации введите номер телефона.
            go: /Auth/AskNumber

        state: Skip
            q: * $skip *
            a: К сожалению, авторизоваться без телефона не получится.
            go!: /Auth/AskNumber